package com.ideas2it.common;

public class Constant {
    public static final String USER_OPINION = "\n1)Press 1 for Employee "
                                               + "management\n"
                                               + "2)Press 2 for Project "
                                               + "management\n"
                                               + "3)Press 3 for Client "
                                               + "management\n"
                                               + "4)Press 4 to exit\n";
    public static final String EMPLOYEE_ID_NOT_FOUND = "Employee Id does not "
                                                           + "exist";
    public static final String PROJECT_ID_NOT_FOUND = "Project Id does not "
                                                          + "exist";
    public static final String EMPLOYEE_ID = "Enter the ID number of employee";
    public static final String EMPLOYEE_NAME = "Enter the employee name";
    public static final String EMPLOYEE_DESIGNATION = "Enter the designation of" 
                                                          + " employee";
    public static final String EMPLOYEE_DOB = "Enter the DOB of employee" 
                                                  + "[DD-MM-YYYY]";
    public static final String EMPLOYEE_MAIL = "Enter the mail ID of employee";

    public static final String USER_OPINION_EMPLOYEE = "\n1)Enter new employee " 
                                                        + "detail\n" 
                                                        + "2)Search employee " 
                                                        + "detail\n"
                                                        + "3)Remove employee "
                                                        + "detail\n" 
                                                        + "4)Display all "
                                                        + "employees data\n" 
                                                        + "5)Update employee "
                                                        + "detail\n" 
                                                        + "6)Press 6 to exit";
    public static final String NO_EMPLOYEES_FOUND = "No employees are present";
    public static final String NO_PROJECTS_FOUND = "No projects found";
    public static final String FAILURE_CREATE = "Failed to add detail";
    public static final String SEARCH_EMPLOYEE = "Enter the employee id "
                                                     + "to get the detail about"
                                                     + " employee";
    public static final String REMOVE_EMPLOYEE = "\nEnter the ID number of"
                                                      + " the employee " 
                                                      + "to remove data";
    public static final String DATA_REMOVED_SUCCESS = "Data corresponding to"
                                                           + " the id removed "
                                                           + "successfully";

    public static final String UPDATE_EMPLOYEE = "\n1)Update name\n"
                                                    + "2)Update Designation \n"
                                                    + "3)Update MailId\n" 
                                                    + "4)Update Date of birth\n"
                                                    + "5)Update Address\n"
                                                    + "6)Press 6 to exit";
    public static final String NEW_EMPLOYEE_NAME = "Enter the new name of "
                                                       + "employee";
    public static final String NEW_EMPLOYEE_DESIGNATION = "Enter the new "
                                                             + "designation of "
                                                             + "employee";
    public static final String NEW_EMPLOYEE_MAIL = "Enter the new mail ID of "
                                                      + "employee";
    public static final String NEW_EMPLOYEE_DOB = "Enter the new DOB of " 
                                                     + "employee [DD-MM-YYYY]";
    public static final String NAME_UPDATED_SUCCES = "Employee name changed "
                                                        + "successfully";
    public static final String DESIGNATION_UPDATED_SUCCESS = "Employee "
                                                             + "designation "
                                                             + "changed "
                                                             + "successfully";
    public static final String MAIL_UPDATED_SUCCESS = "Employee mail changed "
                                                           + "successfully";
    public static final String DOB_UPDATED_SUCCESS ="Employee birth date "
                                                       + "changed successfully";
    public static final String NAME_UPDATED_FAILURE = "Please enter valid name";
    public static final String DESIGNATION_UPDATED_FAILURE ="Please enter valid"
                                                             + " designation";
    public static final String MAIL_UPDATED_FAILURE = "Please enter valid mail";
    public static final String DOB_UPDATED_FAILURE = "Please enter valid DOB";
    public static final String INVALID_INPUT = "Please enter numbers only";
    public static final String INVALID_DATA = "You have entered invalid data";

    public static final String USER_OPINION_PROJECT = "\n1)Enter new project "
                                                      + "detail\n"
                                                      + "2)Search project "
                                                      + "detail\n"
                                                      + "3)Remove project "
                                                      + "detail\n"
                                                      + "4)Display all projects"
                                                      + "\n"
                                                      + "5)Update project "
                                                      + "detail\n"
                                                      + "6)Assign project to "
                                                      + "employee\n"
                                                      + "7)Remove employee from"
                                                      + " project\n"
                                                      + "8)Show employees in "
                                                      + "project\n"
                                                      + "9)Show employees not "
                                                      + "in project\n"
                                                      + "10)Press 10 to exit";
    public static final String PROJECT_ID = "Enter the ID number of project";
    public static final String PROJECT_NAME = "Enter the project name";
    public static final String PROJECT_DOMAIN = "Enter the project domain name";
    public static final String PROJECT_CLIENT = "Enter the project client name";
    public static final String PROJECT_ADDED_SUCCESS = "Project detail added "
                                                           + "successfully";
    public static final String SEARCH_PROJECT = "Enter the project id to get " 
                                                    + "the detail about " 
                                                    + "the project";
    public static final String DELETE_PROJECT = "\nEnter the ID number of the " 
                                                    + "project to remove data";
    public static final String UPDATE_PROJECT = "\n1)Update project name\n"
                                                   + "2)Update project domain\n"
                                                   + "3)Update client name\n"
                                                   + "4)Press 4 to exit\n" ;                                                   
    public static final String NEW_PROJECT_NAME = "Enter the correct name of "
                                                      + "project";
    public static final String NEW_PROJECT_DOMAIN = "Enter the new project " 
                                                        + "domain name";
    public static final String NEW_PROJECT_CLIENT = "Enter the new project "
                                                        + "client name";
    public static final String SUCCESS_ADDED = "Project detail added " 
                                                   + "successfully";
    public static final String PROJECT_REMOVED_SUCCESS = "Project data "
                                                           + "corresponding to "
                                                           + "the id removed "
                                                           + "successfully";
    public static final String PROJECT_NAME_UPDATED_SUCCES = "Project name "
                                                               + "changed "
                                                               + "successfully";
    public static final String PROJECT_CLIENT_UPDATED_SUCCES ="Client name "
                                                               + "changed "
                                                               + "successfully";
    public static final String PROJECT_DOMAIN_UPDATED_SUCCES ="Project domain "
                                                               + "name changed "
                                                               + "successfully";
    public static final String PROJECT_NAME_UPDATED_FAILURE ="Please enter "
                                                                 + "valid name";
    public static final String PROJECT_CLIENT_UPDATED_FAILURE ="Please enter "
                                                                 + "valid "
                                                                 + "client "
                                                                 + "name";
    public static final String PROJECT_DOMAIN_UPDATED_FAILURE = "Please enter "
                                                                 + "valid "
                                                                 + "domain "
                                                                 + "name";
    public static final String ASSIGNED_SUCCESS = "Project assigned to the "
                                                      + "employee successfully";
    public static final String REMOVED_SUCCESS = "Employee removed from project"
                                                      + " successfully";
    public static final String NO_EMPLOYEE_ASSIGNED ="No employees are assigned"
                                                          + " for this project "
                                                          + "or invalid project"
                                                          + " ID";
    public static final String NO_EMPLOYEE_FREE = "None of the employees are "
                                                      + "free";
    public static final String ID_EMPLOYEE = "Employee Id      : ";
    public static final String NAME_EMPLOYEE = "Employee Name    : ";
    public static final String MAIL_EMPLOYEE = "Employee Mail    : ";
    public static final String NEW_LINE = "\n";
    public static final String NO_CONNECTION = "No connection available";
    public static final String TRY_AGAIN = "Something went wrong,"
                                               + " please try again ";
    public static final String DOOR_NO = "Enter the door number";
    public static final String STREET_NAME = "Enter the street name";
    public static final String LAND_MARK = "Enter the landmark area near to "
                                            + "your place";
    public static final String CITY = "Enter the name of city";
    public static final String POSTAL_CODE = "Enter the postal code for your"
                                            + " area";
    public static final String DISTRICT = "Enter district name";
    public static final String STATE = "Enter the state name";
    public static final String COUNTRY = "Enter the country name ";
    
    public static final String CLIENT_ID = "Enter the client Id";
    public static final String CLIENT_NAME = "Enter the client name";
    public static final String CLIENT_MAIL = "Enter the mail ID of client";
    public static final String CLIENT_ORGANISATION = "Enter the "
                                             + "oraganisation name of client";
    public static final String SEARCH_CLIENT = "Enter the client id "
                                                     + "to get the detail about"
                                                     + " client";
    public static final String UPDATE_CLIENT = "\n1)Update client\n"
                                                    + "2)Update address \n"
                                                    + "3)Press 3 to exit\n";
    public static final String REMOVE_CLIENT = "\nEnter the ID number of"
                                                      + " the client " 
                                                      + "to remove data";
    public static final String CLIENT_ID_NOT_FOUND = "Client Id does not "
                                                           + "exist";
    public static final String SUCCESS_CREATE = "The details you have entered "
                                               + " are added successfully";
    public static final String USER_OPINION_CLIENT = "\n1)Enter new client " 
                                                        + "detail\n" 
                                                        + "2)Search client " 
                                                        + "detail\n"
                                                        + "3)Remove client "
                                                        + "detail\n" 
                                                        + "4)Display all "
                                                        + "clients data\n" 
                                                        + "5)Update client "
                                                        + "detail\n"
                                                        + "6)Add project to the"
                                                        + "client\n"  
                                                        + "7)Press 7 to exit";
    public static final String NO_CLIENTS_FOUND = "No clients are present";
}
