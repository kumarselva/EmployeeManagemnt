package com.ideas2it.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.exception.AppException;
import com.ideas2it.model.Client;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Perform all the operations on client 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public interface ClientService {
     
    /**
     * <p>
     * Creating the client profile by adding the informations like Id,name,
     * mailId,name,dob.The client detail will be added if all the informations
     * will be in a valid format.
     * </p>
     * @param clientName                Name of client used for 
     * @param clientMailId              MailId of client 
     * @param clientOrganisation               Organisation name of client
     * @return id number of client if the client details are added 
     */
    boolean createClient(String clientId, String clientName, 
                                   String clientMail,
                                   String clientOrganisation)
                                   throws AppException;     
                                  
    /**
     * <p>
     * Gets the client detail corresponding to the id
     * </p>
     * @param clientId                  clientId to find out the profile of 
     *                                  client
     * @return the client detail corresponding to the id otherwise it 
     * returns null
     */    
    Client searchClientById(String clientId) throws AppException;       
            
    /**
     * <p>
     * Removes the client detail corresponding to the id
     * </p>
     * @param clientId                Id number of client to find out the 
     *                                particular client detail
     * @return true if the client detail is removed otherwise returns false
     */    
    boolean removeClientById(String clientId) throws AppException; 
 
    /**
     * <p>
     * Changing the client profile by adding the new informations like name,
     * mailId,organisation name.The client detail will be added if all the 
     * informations will be in a valid format.
     * </p>
     * @param clientName                Name of client used for 
     * @param clientMailId              MailId of client 
     * @param clientOrganisation               Organisation name of client
     * @return true if the client details are added 
     */            
    void changeClientInfo(String clientId, String clientName, 
                                             String clientMail, 
                                             String clientOrganisation)
                                             throws AppException;          
    /**
     * <p>
     * Gets all clients detail
     * </p>
     * @return the clients list
     */          
    List<Client> getClients() throws AppException;
    
    /**
     * <p>
     * Assigning the project detail to the particular client
     * </p>
     * @param clientId                Id number of client used for 
     *                                searching, deleting and displaying the 
     *                                particular client detail
     * @param projectId               Id number of project used for searching,
     *                                deleting,displaying the particular project
     *                                detail
     */    
    void assignProjectToClient(String clientId, String projectId) 
                                           throws AppException;
    /**
     * <p>
     * Find the presence of client using the client Id
     * </p>
     * @param clientId         Id number of client
     * @return true if the id of client already exists else return false
     */  
    boolean isClientPresent(String clientId) throws AppException;               
    
 }                       
                      
