package com.ideas2it.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.ClientDao;
import com.ideas2it.dao.impl.ClientDaoImpl;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Client;
import com.ideas2it.service.ClientService;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Performs searching, updating, displaying, deleting operations on client 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public class ClientServiceImpl implements ClientService {
    private ClientDao clientDao = new ClientDaoImpl();
                
    /**
     * @see com.ideas2it.service.ClientService;
     * #boolean createClient(String clientId, 
     *                            String clientDesignation, 
     *                            String clientMailId, String clientName, 
     *                            String dateOfBirth)
     */
    public boolean createClient(String clientId, String clientName, 
                                  String clientMail, 
                                  String clientOrganisation) 
                                  throws AppException {    
        if (validateClient(clientId, clientName, clientMail)) {    
            Client client = new Client(clientId, clientName, clientMail, 
                                            clientOrganisation);
            return clientDao.insertClient(client);
        }
        return false;   
    }
    
    /**
     * @see com.ideas2it.service.ClientService;
     * #Client searchClientById(String clientId)
     */      
    public Client searchClientById(String clientId) throws AppException {      
        return clientDao.getClient(clientId);
    }
    
    /**
     * @see com.ideas2it.service.ClientService;
     * #boolean removeClientById(String clientId)
     */          
    public boolean removeClientById(String clientId) throws AppException {
        if (isClientPresent(clientId)) {
            clientDao.deleteClient(clientId);
            return true; 
        } 
        return false;
    }
    
    /**
     * @see com.ideas2it.service.ClientService
     * public void changeClientInfo(String clientName, String clientMail, 
     *                                        String clientOrganisation)        
     */   
    public void changeClientInfo(String clientId, String clientName, 
                                             String clientMail, 
                                             String clientOrganisation)
                                             throws AppException {
        clientDao.updateClientInfo(new Client(clientId, clientName, clientMail, 
                                       clientOrganisation));
    }         
     
    /**
     * @see com.ideas2it.service.ClientService;
     * #List<Client> getClients()
     */         
    public List<Client> getClients() throws AppException {
        return clientDao.retrieveClients();            
    }
    
    /**
     * @see com.ideas2it.service.ClientService;
     * #void assignProjectToClient(clientId, projectId)
     */             
    public void assignProjectToClient(String clientId, String projectId) 
                                           throws AppException {
          clientDao.assignProjectToClient(clientId, projectId);
    }
    
    /**
     * @see com.ideas2it.service.ClientService;
     * #boolean isClientPresent(String clientId)
     */          
    public boolean isClientPresent(String clientId) throws AppException {
        Client client = searchClientById(clientId);
        if (null != client) {
            return true;
        }
        return false;
    }
    
    /**
     * <p>
     * Validate the client detail like name, mail Id, dob,designation
     * </p>
     * @param clientName            Name of the client
     * @param clientMail            Mail of client
     * @param clientOrganisation    Organisation name of client
     * @return true if all the details are in valid format
     */
    private boolean validateClient(String clientId, String clientName, 
                                       String clientMail) { 
        return (isValidClientId(clientId) && isValidClientName(clientName)
                                      && isValidClientMail(clientMail));
    }
    
    /**
     * <p>
     * Validating client name.It should start with uppercase and couldn't have
     * any numbers 
     * </p>
     * @param clientName          Name of client
     * Valid : Kumar
     * Invalid : kumar123, Kumar.G, Kumar G 
     * @return true if the client name is in valid format
     */
    private boolean isValidClientId(String id) {
        return CommonUtil.isValidClientId(id);
    }

    /**
     * <p>
     * Validating client name.It should start with uppercase and couldn't have
     * any numbers 
     * </p>
     * @param clientName          Name of client
     * Valid : Kumar
     * Invalid : kumar123, Kumar.G, Kumar G 
     * @return true if the client name is in valid format
     */
    private boolean isValidClientName(String name) {
        return CommonUtil.isValidName(name);
    }
    
    /**
     * <p> 
     * Validating mail Id of client.It should not have any uppercase 
     * letters and space characters.
     * </p>
     * @param mail        MailId of client
     * Valid : ideas2it@gmail.com
     * Invalid:Ideas2it15.com
     * @return true if the mail id is in valid format
     */
    private boolean isValidClientMail(String mail) {
        return CommonUtil.isValidMailId(mail);
    }    
}                      
