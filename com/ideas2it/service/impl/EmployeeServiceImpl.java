package com.ideas2it.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.EmployeeDao;
import com.ideas2it.dao.impl.EmployeeDaoImpl;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Employee;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Performs searching, updating, displaying, deleting operations on employee 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDao employeeDao = new EmployeeDaoImpl();
                
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean createEmployee(String employeeId, 
     *                            String employeeDesignation, 
     *                            String employeeMailId, String employeeName, 
     *                            String dateOfBirth)
     */
    public boolean createEmployee(String employeeId, String employeeDesignation, 
                                  String employeeMailId, String employeeName, 
                                  String dateOfBirth) throws AppException {    
        if ((!isEmployeePresent(employeeId)) && validateEmployee(employeeId, 
               employeeDesignation, employeeMailId, employeeName, 
               dateOfBirth)) {    
            Employee employee = new Employee(employeeId, 
                                                employeeDesignation, 
                                                employeeMailId, 
                                                employeeName, 
                                                dateOfBirth);
            employeeDao.insertEmployee(employee);
            return true;
        }
        return false;   
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #Employee searchEmployeeById(String employeeId)
     */      
    public Employee searchEmployeeById(String employeeId) throws AppException {      
        if(isValidEmployeeId(employeeId)) {
           return employeeDao.getEmployee(employeeId);
        }
        return null;
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean removeEmployeeById(String employeeId)
     */          
    public boolean removeEmployeeById(String employeeId) throws AppException {
        if (isEmployeePresent(employeeId)) {
            employeeDao.deleteEmployee(employeeId);
            return true; 
        } 
        return false;
    }    
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean changeName(String employeeId, String newName)
     */              
    public boolean changeName(String employeeId, String newName) 
                                     throws AppException {
        if (isValidEmployeeName(newName)) {    
            employeeDao.updateEmployeeName(employeeId, newName);           
            return true;                                                   
        }
        return false;
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean changeDesignation(String employeeId, 
     * String newDesignation)
     */                            
    public boolean changeDesignation(String employeeId, String newDesignation)
                                     throws AppException {
        if (isValidEmployeeDesignation(newDesignation)) {         
            employeeDao.updateEmployeeDesignation(employeeId, newDesignation);                                               
            return true;
        }
        return false;
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean changeMail(String employeeId, String newMail)
     */      
    public boolean changeMail(String employeeId, String newMail) 
                                      throws AppException {
        if (isValidEmployeeMailId(newMail)) {
            employeeDao.updateEmployeeMail(employeeId, newMail);                                                         
            return true;
        }
        return false;
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean changeDateOfBirth(String employeeId, String newDob)
     */          
    public boolean changeDateOfBirth(String employeeId, String newDob)
                                         throws AppException {
        if (isValidDateOfBirth(newDob)) {
            employeeDao.updateEmployeeDob(employeeId, newDob);                                                
            return true;
        } 
        return false;
     } 
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #List<Employee> getEmployees()
     */         
    public List<Employee> getEmployees() throws AppException {
        return employeeDao.retrieveEmployees();            
    }
    
    /**
     * @see com.ideas2it.service.EmployeeService;
     * #boolean isEmployeePresent(String employeeId)
     */          
    public boolean isEmployeePresent(String employeeId) throws AppException {
        Employee employee = searchEmployeeById(employeeId);
        if (null != employee) {
            return true;
        }
        return false;
    }
    
    /**
     * <p>
     * Validate the employee detail like name, mail Id, dob,designation
     * </p>
     * @param employeeId            Id number of employee used for updating,
     *                              searching and displaying the particular
     *                              employee profile
     * @param employeeDesignation   Designation name of employee
     * @param employeeMailId        Mail Id of employee
     * @param employeeName          Name of employee
     * @param dateOfBirth           Birthdate of employee
     * @return true if all the details are in valid format
     */
    private boolean validateEmployee(String employeeId, 
                                       String employeeDesignation, 
                                       String employeeMailId,
                                       String employeeName, 
                                       String dateOfBirth) { 
        return (isValidEmployeeId(employeeId)
                   && isValidEmployeeDesignation(employeeDesignation) 
                   && isValidEmployeeMailId(employeeMailId) 
                   && isValidEmployeeName(employeeName)
                   && isValidDateOfBirth(dateOfBirth));
    }
    
    /**
     * <p>
     * Validating employeeId,It should be in numbers only 
     * </p>
     * @param employeeId            Id number of employee       
     * 4 digit number Ex:1234
     * @return true if all the employee Id is in valid format
     */
    private boolean isValidEmployeeId(String id) {
        return CommonUtil.isValidEmployeeId(id);
    }

    /**
     * <p>
     * Validating employee name.It should start with uppercase and couldn't have
     * any numbers 
     * </p>
     * @param employeeName          Name of employee
     * Valid : Kumar
     * Invalid : kumar123, Kumar.G, Kumar G 
     * @return true if the employee name is in valid format
     */
    private boolean isValidEmployeeName(String name) {
        return CommonUtil.isValidName(name);
    }
    
    /**
     * <p>
     * Validating employee designation.It should start with uppercase letter
     * </p>
     * @param employeeDesignation   Designation of the employee
     * Valid : Software Developer, Police
     * Invalid : Software developer,po12
     * @return true if the employee designation is in valid format
     */
    private boolean isValidEmployeeDesignation(String designation) {
        return CommonUtil.isValidName(designation);
    }
    
    /**
     * <p> 
     * Validating mail Id of employee.It should not have any uppercase 
     * letters and space characters.
     * </p>
     * @param mail        MailId of employee
     * Valid : ideas2it@gmail.com
     * Invalid:Ideas2it15.com
     * @return true if the mail id is in valid format
     */
    private boolean isValidEmployeeMailId(String mail) {
        return CommonUtil.isValidMailId(mail);
    }
    
    /**
     * <p>
     * Validating Date Of Birth It should be in DD-MM-YYYY format only
     * </p>
     * @param dob        Date of birth of employee
     * Valid : 04-08-1996
     * Invalid:09/08/1264, 1996-04-08
     * @return true if date of birth is in valid format
     */
    private boolean isValidDateOfBirth(String dob) {
        return CommonUtil.isValidDateOfBirth(dob);
    }                   
}                      
