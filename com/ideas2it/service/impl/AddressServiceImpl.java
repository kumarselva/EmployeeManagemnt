package com.ideas2it.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.AddressDao;
import com.ideas2it.dao.impl.AddressDaoImpl;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Address;
import com.ideas2it.service.AddressService;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Perform searching, updating, displaying, deleting operations on address 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public class AddressServiceImpl implements AddressService {
    private AddressDao addressDao = new AddressDaoImpl();
                
    /**
     * @see com.ideas2it.service.AddressService;
     * #boolean createAddress(String doorNo, String streetName,
     *                           String landMark,String city, String postalCode, 
     *                           String district, String state, String country,
     *                           String Id) throws AppException;     
     */
    public boolean createAddress (String doorNo, String streetName, 
                                String landMark, String city, String postalCode,
                                String district, String state, String country,
                                String Id) throws AppException {              
        Address address = new Address (doorNo, streetName, landMark, city, 
                                          postalCode, district, state, country);                
        return addressDao.insertAddress(address, Id);   
    }

    /**
     * @see com.ideas2it.service.AddressService;
     * #boolean changeAddress(String doorNo, String streetName,
     *                           String landMark,String city, String postalCode, 
     *                           String district, String state, String country,
     *                           String Id) throws AppException;     
     */
    public boolean changeAddress (String doorNo, String streetName, 
                                String landMark, String city, String postalCode,
                                String district, String state, String country,
                                String Id) throws AppException {              
        Address address = new Address (doorNo, streetName, landMark, city, 
                                          postalCode, district, state, country);                
        return addressDao.updateAddress(address, Id);   
    }
    
    /**
     * @see com.ideas2it.service.AddressService;
     * #boolean removeAddressById(String Id)
     */          
    public boolean removeAddressById(String Id) throws AppException {
            return addressDao.deleteAddress(Id);
    }    

    /**
     * @see com.ideas2it.service.AddressService;
     * #boolean searchAddressById(String Id)
     */          
    public List<Address> searchAddressById(String Id) throws AppException {
            return addressDao.getAddress(Id);
    }    
}
