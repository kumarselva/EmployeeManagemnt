package com.ideas2it.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.ProjectDao;
import com.ideas2it.dao.impl.ProjectDaoImpl;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.ProjectService;
import com.ideas2it.util.CommonUtil;


/**
 * <p>
 * Performs searching, updating, displaying, deleting operations on the project
 * </p> 
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public class ProjectServiceImpl implements ProjectService {
    private ProjectDao projectDao = new ProjectDaoImpl();
    
    /**
     * @See com.ideas2it.service.ProjectService;
     * #boolean createProject(String projectId, String projectName, 
     *                                 String projectDomain, 
     *                                 String projectClient)
     */
    public boolean createProject(String projectId, String projectName, 
                                  String projectDomain) throws AppException {                              
        if ((!isProjectPresent(projectId)) && validateProject(projectId, 
               projectName, projectDomain)) {    
            Project project = new Project(projectId, projectName, 
                                                projectDomain);
            projectDao.insertProject(project);
            return true;
        } 
        return false;   
    }
    
    /*
     * @@See com.ideas2it.service.ProjectService;
     * #Project searchProjectById(String projectId)
     */
    public Project searchProjectById(String projectId) throws AppException {             
        if (isValidProjectId(projectId)) {
            return projectDao.getProject(projectId);
        }
        return null;
    }

    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean assignProject(String projectId, String employeeId)
     */
    public boolean assignProjectToEmployee(String projectId, String employeeId) 
                                    throws AppException {        
       if (isEmployeePresent(employeeId)) { 
           projectDao.assignProjectToEmployee(projectId, employeeId);
           return true;
       }
       return false;
    }
    
    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean removeEmployeeFromProject(String projectId, 
     *                                          String employeeId) 
     */
    public boolean removeEmployeeFromProject(String projectId, 
                                                String employeeId)  
                                                throws AppException {
        if (isEmployeePresent(employeeId)) {        
            projectDao.deleteEmployeeFromProject(projectId, employeeId);
            return true;
        }
        return false;
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean isEmployeePresent(String employeeId)
     */    
    public boolean isEmployeePresent(String employeeId) throws AppException {
        EmployeeService employeeService = new EmployeeServiceImpl();
        if (employeeService.isEmployeePresent(employeeId)) {
            return true;
        } 
        return false;
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean removeProjectById(String projectId) 
     */        
    public boolean removeProjectById(String projectId)  
                                     throws AppException {
       if (isProjectPresent(projectId)) {
           
           projectDao.deleteProject(projectId);
           return true; 
       }
       return false;   
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean changeName(String projectId, String newName)
     */         
    public boolean changeName(String projectId, String newName) 
                                    throws AppException {
        if (isValidProjectName(newName)) {         
            projectDao.updateProjectName(projectId, newName);                                                   
            return true;
        }
        return false;
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean changeDomain(String projectId, String newDomain)
     */    
    public boolean changeDomain(String projectId, String newDomain)  
                                     throws AppException {
        if (isValidProjectDomain(newDomain)) {    
            projectDao.updateProjectDomain(projectId, newDomain);
            return true;                                                   
        }
        return false;    
    }                   
      
    /*
     * @See com.ideas2it.service.ProjectService;
     * #List<Project> getProjects()
     */                   
    public List<Project> getProjects() throws AppException {
        return projectDao.retrieveProjects();        
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * #boolean isProjectPresent(String projectId)
     */        
    public boolean isProjectPresent(String projectId) throws AppException {
        Project project = searchProjectById(projectId);
        if (null != project) {
            return true;
        }
        return false;
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * # public List<Employee> getEmployeesInProject(String projectId)
     */       
    public List<Employee> getEmployeesInProject(String projectId) 
                                     throws AppException {
       if (isProjectPresent(projectId)) {
           return projectDao.getEmployeesInProject(projectId);
       }
       return null;
    }
   
    /*
     * @See com.ideas2it.service.ProjectService;
     * #List<List<String>> getEmployeesNotInProject()
     */        
    public List<List<String>> getEmployeesNotInProject() throws AppException {
        return  projectDao.getEmployeesNotInProject();
    }
    
    /**
     * <p>
     * Validate project details
     * </p>
     * @param projectId            Id number of project
     * @param projectName          Name of project
     * @param projectDomain        Domain of project
     */
    private boolean validateProject(String projectId, 
                                       String projectName, 
                                       String projectDomain) { 
        return (isValidProjectId(projectId) && isValidProjectName(projectName)  
                    && isValidProjectDomain(projectDomain));
    }
    
    /**
     * <p>
     * Validate projectId.It should be a number.
     * </p>
     * @param Id            Id number of project       
     * 4 digit number Ex:1234
     * @return true if the id is in proper format
     */
    private boolean isValidProjectId(String id) {
        return CommonUtil.isValidId(id);
    }

    /**
     * <p>
     * Validate projectDomain.It should starts with uppercase letter
     * </p>
     * @param Domain          Domain of project
     * Valid : Database
     * Invalid : database
     * @return true if the domain name is in proper format
     */
    private boolean isValidProjectDomain(String domain) {
        return CommonUtil.isValidName(domain);
    }
    
    /**
     * <p>
     * Validate projectName.It should starts with uppercase letter
     * </p>
     * @param Name   Name of the project
     * Valid : Webdesign,
     * Invalid : webdesign
     * @return true if the project name is in proper format
     */
    private boolean isValidProjectName(String name) {
        return CommonUtil.isValidName(name);
    }    
}

