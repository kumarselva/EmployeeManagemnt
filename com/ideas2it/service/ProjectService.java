package com.ideas2it.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.dao.ProjectDao;
import com.ideas2it.dao.impl.ProjectDaoImpl;
import com.ideas2it.exception.AppException;;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.util.CommonUtil;


/**
 * <p>
 * Perform operations on the project
 * </p> 
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public interface ProjectService {
    
    /**
     * <p>
     * Creating the project detail if the project Id does not exist and all the 
     * project details like Id,domain are valid
     * </p>
     * @param projectId                Id number of project used for searching,
     *                                 changing,deleting and displaying the 
     *                                 particular project detail
     * @param projectName              Name of the project
     * @param projectDomain            Domain name of project 
     * @return true if the project detail is added
     */
    boolean createProject(String projectId, String projectName, 
                                  String projectDomain) throws AppException;                               
        
    /**
     * <p>
     * Getting the project detail by using the project id
     * </p>
     * @param projectId                projectId to find the detail of 
     *                                 project
     * @return the project detail corresponding to the id
     * @return null if no projects are present by this id
     */    
    Project searchProjectById(String projectId) throws AppException;              

    /**
     * <p>
     * Assigning project to employee by using the employee Id
     * </p>
     * @param projectId                projectId to find the detail of 
     *                                 project
     * @param employeeeId              Id number of employee to get the detail 
     *                                 of employee
     * @returns true if the project is assigned to the employee else returns 
     * false
     */    
    boolean assignProjectToEmployee(String projectId, String employeeId) 
                                    throws AppException;         
        
    /**
     * <p>
     * Removing the employee from project with respect to the project and 
     * employee Id
     * </p>
     * @param projectId                projectId to find the detail of 
     *                                 project
     * @param employeeeId              Id number of employee to get the detail 
     *                                 about employee
     * @returns true if the employee is removed from project else returns false
     */    
    boolean removeEmployeeFromProject(String projectId, 
                                                String employeeId)  
                                                throws AppException; 

    /**
     * <p>
     * Searching the presence of employee using the employee id
     * </p>
     * @param employeeId                Id number of employee to get the 
     *                                  employee profile
     * @return true if the employeeId exists
     */   
    boolean isEmployeePresent(String employeeId) throws AppException; 
    
    
    /**
     * <p>
     * Removing the project detail corresponding to the project id
     * </p>
     * @param projectId                Project Id number used to get the detail 
     *                                 about project
     * @return true if the project detail is deleted
     */    
    boolean removeProjectById(String projectId)  
                                     throws AppException; 
         
    /**
     * <p>
     * Change project name by using the projectId
     * </p>
     * @param projectId                project Id to get the corresponding
     *                                 details about project
     * @param newName               New name of the project
     * @return true if the project name is changed
     * @returns false if the project name is invalid
     */
    boolean changeName(String projectId, String newName) 
                                    throws AppException; 
    
    /**
     * <p>
     * Change project domain by using the project Id
     * </p>
     * @param projectId                Project Id to get the corresponding
     *                                 details about project
     * @param newDomain                New domain name of the project
     * @return true if the project domain name is changed
     * @returns false if the domain name is invalid
     */
    boolean changeDomain(String projectId, String newDomain)  
                                     throws AppException; 
                         
    /**
     * <p>
     * Display details of all projects
     * </p>
     * @returns all projects list
     */          
    List<Project> getProjects() throws AppException; 
       
    /**
     * <p>
     * Find the presence of project by using the project Id
     * </p>
     * @param projectId              Id number of project to get the detail 
     *                               about project
     * @return true if the id of project already exists
     */  
    boolean isProjectPresent(String projectId) throws AppException; 
 
    
    /**
     * <p>
     * Getting employees detail who are in a particular project by using 
     * the project Id
     * </p>
     * @param projectId              Id number of project
     * @return the employees detail corresponding to the project
     */  
    List<Employee> getEmployeesInProject(String projectId) 
                                     throws AppException; 
        
    /**
     * <p>
     * Getting employees detail who are in not in any project
     * </p>
     * @return the employees detail Who are not involved in any project
     */  
    List<List<String>> getEmployeesNotInProject() throws AppException; 
    
 }

