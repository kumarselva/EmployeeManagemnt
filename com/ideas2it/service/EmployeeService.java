package com.ideas2it.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.exception.AppException;
import com.ideas2it.model.Employee;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Perform all the operations on employee 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public interface EmployeeService {
     
    /**
     * <p>
     * Creating the employee profile by adding the informations like Id,name,
     * mailId,name,dob.The employee detail will be added if all the informations
     * will be in a valid format.
     * </p>
     * @param employeeId                Id number of employee used for 
     *                                  searching,updating,deleting the employee
     *                                  profile
     * @param employeeDesignation       Designation of employee
     * @param employeeMailId            MailId of employee 
     * @param employeeName              Name of employee
     * @param dateOfBirth               Birth date of employee
     * @return true if the employee details are added 
     */
    boolean createEmployee(String employeeId, String employeeDesignation, 
                                  String employeeMailId, String employeeName, 
                                  String dateOfBirth) throws AppException;     
                                  
    /**
     * <p>
     * Gets the employee detail corresponding to the id
     * </p>
     * @param employeeId                employeeId to find out the profile of 
     *                                  employee
     * @return the employee detail corresponding to the id otherwise it 
     * returns null
     */    
    Employee searchEmployeeById(String employeeId) throws AppException;       
            
    /**
     * <p>
     * Removes the employee detail corresponding to the id
     * </p>
     * @param employeeId                Id number of employee to find out the 
     *                                  particular employee detail
     * @return true if the employee detail is removed otherwise returns false
     */    
    boolean removeEmployeeById(String employeeId) throws AppException; 
             
    /**
     * <p>
     * Change employee name by using the employeeId
     * </p>
     * @param employeeId                Employee Id number to get the 
                                        details about employee
     * @param newName                   The name which replace the existing name
     *                                  of the employee
     * @return true if the employee name is changed
     * @return false if the name is in invalid format
     */
    boolean changeName(String employeeId, String newName) 
                                     throws AppException; 
                     
    /**
     * <p>
     * Changing the employee designation by using the employeeId
     * </p>
     * @param employeeId                Employee Id number to get the 
     *                                  details about employee
     * @param newDesignation                   New designation of employee
     * @return true if the employee designation is changed
     * @return false if the designation is in invalid format
     */
    boolean changeDesignation(String employeeId, String newDesignation)
                                     throws AppException; 
            
    /**
     * <p>
     * Changing the employee mail Id by using the employeeId
     * </p>
     * @param employeeId                Employee Id number to get the 
     *                                  details about employee
     * @param newMail                   New mail Id of employee
     * @return true if the employee mail Id is changed
     * @return false if the mail Id is in invalid format
     */
    boolean changeMail(String employeeId, String newMail) 
                                      throws AppException; 
           
    /**
     * <p>
     * Changing the employee date of birth by using the employeeId
     * </p>
     * @param employeeId                Employee Id number to get the 
     *                                  details about employee
     * @param newDob                    It replace the existing dob of employee
     * @return true if the employee dob is changed
     * @return false if the dob is in invalid format
     */
    boolean changeDateOfBirth(String employeeId, String newDob)
                                         throws AppException; 
               
    /**
     * <p>
     * Gets all employees detail
     * </p>
     * @return the employees list
     */          
    List<Employee> getEmployees() throws AppException;
    
    /**
     * <p>
     * Find the presence of employee using the employee Id
     * </p>
     * @param employeeId         Id number of employee
     * @return true if the id of employee already exists else return false
     */  
    boolean isEmployeePresent(String employeeId) throws AppException;               
    
 }                       
                      
