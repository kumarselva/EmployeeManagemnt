package com.ideas2it.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.ideas2it.exception.AppException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Employee;
import com.ideas2it.util.CommonUtil;

/**
 * <p>
 * Performs searching, updating, displaying, deleting operations on address 
 * </p>
 * @author Selvakumar
 * @Created on 13/07/2017
 */
public interface AddressService {
     
    /**
     * <p>
     * Creating the the address of employee by passing the information
     * </p>
     * @param doorNo                    door number of employee
     * @param street                    Street name of employee
     * @param landmark                  Important place near to the employee's
     *                                  living area 
     * @param city                      Name of city
     * @param postalCode                Postal number of the city
     * @param district                  Name of the district where the person is
     *                                  living
     * @param state                     Name of the state where the person is
     *                                  living
     * @param country                   Name of the country where the person is 
     *                                  currently living
     * @param employeeId                Employee id number used for searching,
     *                                  removing and displaying the employee 
     *                                  detail
     */
    boolean createAddress(String doorNo, String streetName,
                                 String landMark,String city, String postalCode, 
                                 String district, String state, String country,
                                 String Id) throws AppException;
    
   /**
     * <p>
     * Changing the the address of employee by passing the information
     * </p>
     * @param doorNo                    New door number of employee
     * @param street                    Street name of employee
     * @param landmark                  Important place near to the employee's
     *                                  living area 
     * @param city                      Name of city
     * @param postalCode                Postal number of the city
     * @param district                  Name of the district where the person is
     *                                  living
     * @param state                     Name of the state where the person is
     *                                  living
     * @param country                   Name of the country where the person is 
     *                                  currently living
     * @param employeeId                Employee id number used for searching,
     *                                  removing and displaying the employee 
     *                                  detail
     */
    boolean changeAddress(String doorNo, String streetName,
                                 String landMark,String city, String postalCode, 
                                 String district, String state, String country,
                                 String Id) throws AppException;
    /**
     * <p>
     * Removes the address detail corresponding to the id
     * </p>
     * @param Id                        Id number of person used to find out the 
     *                                  particular person's address
     * @return true if the address is removed otherwise returns false
     */    
    boolean removeAddressById(String Id) throws AppException; 

    /**
     * <p>
     * Removes the address detail corresponding to the id
     * </p>
     * @param Id                        Id number of person used to find out the 
     *                                  particular person's address
     * @return true if the address is removed otherwise returns false
     */        
    List<Address> searchAddressById(String Id) throws AppException;
  
 }                       

