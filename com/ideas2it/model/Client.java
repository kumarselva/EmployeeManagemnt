package com.ideas2it.model;

/**
 * <p>
 * This class is used to get and set the records about the particular client 
 * </p>
 * @author Selvakumar
 */
public class Client {
    private String id;
    private String name;
    private String mail;
    private String organisation;
    
    public Client (String id, String name, String mail, 
                      String organisation) {                 
        this.id = id;
        this.name = name;
        this.mail = mail;
        this.organisation = organisation;
    }

    public String getId() {
        return id;
    }
            
    public String getName() {
        return name;
    }
        
    public String getMail() {
        return mail;
    }
   
    public String getOrganisation() {
        return organisation;
    }
    

    public void setId(String id) {
        this.id = id;
    }
   
    public void setName(String name) {
        this.name = name;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }
    public String toString() {
        return "\nClient Name           : " + name
                   + "\nClient Mail           : " + mail
                   + "\nClient Organisation   : " + organisation +"\n";
    }
}
