package com.ideas2it.model;

import com.ideas2it.util.DateUtil;

/**
 * <p>
 * This class is used to get and set the records about the particular employee 
 * </p>
 * @author Selvakumar
 */
public class Employee {
    private String id;
    private String designation;
    private String mailId;
    private String name;
    private String dob;
    private int age; 
    
    public Employee (String id, String designation,
                     String mailId, String name, String dob) {                 
        this.id = id;
        this.designation = designation;
        this.mailId = mailId;
        this.name = name;
        this.dob = dob;
    }
    
    public String getId() {
        return id;
    }
    
    public String getDesignation() {
        return designation;
    }
    
    public String getMailId() {
        return mailId;
    }
   
    public String getName() {
        return name;
    }
    
    public String getDateOfBirth() {
        return dob;
    }
    
    public int getAge() {
        return DateUtil.calculateAge(dob);
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setDateOfBirth(String dateOfBirth) {
        this.dob = dateOfBirth;
    }
    
    public void setAge(String dob) {
        this.age = DateUtil.calculateAge(dob);     
    } 
    
    public String toString() {
        return "\nEmployee Id            : " + id
                   + "\nEmployee Name          : " + name
                   + "\nEmployee age           : " + getAge()
                   + "\nEmployee Designation   : " + designation
                   + "\nEmployee Mail Id       : " + mailId +"\n";
    }
}
