package com.ideas2it.model;

/**
 * <p>
 * This class is used to get and set the records about the particular person's
 * address 
 * </p>
 * @author Selvakumar
 */
public class Address {
    private String doorNo;
    private String streetName;
    private String landMark;
    private String city;
    private String postalCode;
    private String district;
    private String state;
    private String country;
    
    public Address (String doorNo, String streetName, String landMark,
                       String city, String postalCode, String district, 
                       String state, String country) {                 
        this.doorNo = doorNo;
        this.streetName = streetName;
        this.landMark = landMark;
        this.city = city;
        this.postalCode = postalCode;
        this.district = district;
        this.state = state;
        this.country = country;
    }
    
    /**
     * <p>
     * Getter and setter methods
     * </p>
     */
    public String getDoorNo() {
        return doorNo;
    }
    
    public String getStreetName() {
        return streetName;
    }
    
    public String getLandMark() {
        return landMark;
    }
   
    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }
    
    public String getDistrict() {
        return district;
    }
    
    public String getState() {
        return state;
    }
    
    public String getCountry() {
        return country;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }
    
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
    
    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }
   
    public void setCity(String city) {
        this.city = city;
    }
    public void setPostalCode(String postCode) {
        this.postalCode = postalCode;
    }
    
    public void setDistrict(String district) {
        this.district = district;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
     
    public String toString() {
        return "\nDoorNo & StreetName : " + doorNo + ", " + streetName
                    + "\nLand Mark           : " + landMark
                    + "\nCity                : " + city + " - " + postalCode
                    + "\nDistrict            : " + district 
                    + "\nState               : " + state
                    + "\nCountry             : " + country +"\n";
    }
}

