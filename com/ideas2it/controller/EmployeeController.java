package com.ideas2it.controller;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.common.Constant;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Employee;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.view.EmployeeView;

/**
 * <p>
 * Control the flow of operations between the view and service class
 * </p> 
 */
public class EmployeeController {
    private EmployeeService employeeService = new EmployeeServiceImpl();
      
    /**
     * <p>
     * Creating the the employee detail by passing the information to the 
     * service class
     * </p>
     * @param employeeId                Id number of employee used for 
     *                                  searching, deleting and displaying the 
     *                                  particular employee detail
     * @param employeeDesignation       Designation of employee
     * @param employeeMailId            MailId of employee and it is unique
     * @param employeeName              Name of employee
     * @param employeeDob               Birth date of employee
     */
    public boolean createEmployee(String employeeId, String employeeDesignation, 
                                String employeeMailId, String employeeName, 
                                String employeeDob) {
        EmployeeView employeeView = new EmployeeView();
        try {
             return employeeService.createEmployee(employeeId, 
                                                   employeeDesignation, 
                                                   employeeMailId, employeeName, 
                                                   employeeDob); 
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return false;
    }
    
    /**
     * <p>
     * Creating the the address of employee by passing the information to the 
     * service class
     * </p>
     * @param doorNo                    door number of employee
     * @param street                    Street name of employee
     * @param landmark                  Important place near to the employee's
     *                                  living area 
     * @param city                      Name of city
     * @param postalCode                Postal number of the city
     * @param district                  Name of the district where the person is
     *                                  living
     * @param state                     Name of the state where the person is
     *                                  living
     * @param country                   Name of the country where the person is 
     *                                  currently living
     * @param employeeId                Employee id number used for searching,
     *                                  removing and displaying the employee 
     *                                  detail
     */
    public boolean createAddress(String doorNo, String street, String  landMark, 
                                 String city, String postalCode,
                                 String district, String state,
                                 String country, String employeeId) {
        AddressService addressService = new AddressServiceImpl();
        EmployeeView employeeView = new EmployeeView();       
        try {
            return addressService.createAddress(doorNo, street, landMark, city, 
                                             postalCode, district, state, 
                                             country,employeeId);
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return false;
    }

    /**
     * <p>
     * Changing the the address of employee by passing the information to the 
     * service class
     * </p>
     * @param doorNo                    Door number which replace the current 
     *                                  door number of employee
     * @param street                    New street name of employee
     * @param landmark                  Important place near to the employee's
     *                                  living area 
     * @param city                      Name of city
     * @param postalCode                Postal number of the city
     * @param district                  Name of the district where the person is
     *                                  living
     * @param state                     Name of the state where the person is
     *                                  living
     * @param country                   Name of the country where the person is 
     *                                  currently living
     * @param employeeId                Employee id number used for searching,
     *                                  removing and displaying the employee 
     *                                  detail
     */
    public boolean changeAddress(String doorNo, String street, String  landMark, 
                                 String city, String postalCode,
                                 String district, String state,
                                 String country, String employeeId) {
        AddressService addressService = new AddressServiceImpl();
        EmployeeView employeeView = new EmployeeView();
        try {
            return addressService.changeAddress(doorNo, street, landMark, city, 
                                             postalCode, district, state, 
                                             country,employeeId);
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return false;
    }
     
    /**
     * <p>
     * searching employee profile by using the employee Id
     * </p>
     * @param employeeId                Id number of employee used for 
     *                                  searching and displaying the 
     *                                  particular employee detail
     */   
    public Employee getEmployeeById(String employeeId){
        EmployeeView employeeView = new EmployeeView();
        try {
            return employeeService.searchEmployeeById(employeeId);
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return null;
    }    

    /**
     * <p>
     * searching address detail of employee by using the employee Id
     * </p>
     * @param employeeId                Id number of employee used for 
     *                                  searching and displaying the 
     *                                  particular employee detail
     */   
    public List<Address> getAddressById(String employeeId){
        AddressService addressService = new AddressServiceImpl();
        EmployeeView employeeView = new EmployeeView();
        try {
            return addressService.searchAddressById(employeeId);
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return null;
    }    
    
    /**
     * <p>
     * Removing employee profile using employee Id
     * </p>
     * @param employeeId                Id number of employee used for 
     *                                  searching and deleting the 
     *                                  particular employee detail
     */
    public boolean removeEmployeeById(String employeeId) {
        AddressService addressService = new AddressServiceImpl();
        EmployeeView employeeView = new EmployeeView();
        try {
            return (addressService.removeAddressById(employeeId) && 
                       employeeService.removeEmployeeById(employeeId));
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return false;
    }
            
    /**
     * <p>
     * Dispaly all employee details with all information which includes employee
     * name, Id number, designation, mail id and their date of birth
     * </p>
     */
    public List<Employee> getEmployees() {
        EmployeeView employeeView = new EmployeeView();
        try {
            return employeeService.getEmployees();
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return null;
    }
        
    /**
     * <p>
     * Made changes in the name of employee by using the id
     * </p>
     * @param employeeId      Id number of employee used to get the particular 
     *                        employee detail
     * @param newName         The name which replace the old name of employee
     */
    public void changeName(String employeeId, String newName) {
        EmployeeView employeeView = new EmployeeView();
        try {
            if (employeeService.changeName(employeeId, newName)) {
                    employeeView.printMessage(Constant.NAME_UPDATED_SUCCES);
            } else {
                employeeView.printMessage(Constant.NAME_UPDATED_FAILURE);
            }
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
    }

    /**
     * <p>
     * Made changes in the designation of employee by using the id
     * </p>
     * @param employeeId        Id number of employee used to get the particular 
     *                          employee detail
     * @param newDesignation    New designation of employee
     */   
    public void changeDesignation(String employeeId, String newDesignation) {
        EmployeeView employeeView = new EmployeeView();
        try {
            if (employeeService.changeDesignation(employeeId, newDesignation)) {
                employeeView.printMessage(Constant.DESIGNATION_UPDATED_SUCCESS);
            } else {
                employeeView.printMessage(Constant.DESIGNATION_UPDATED_FAILURE);
            }
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /**
     * <p>
     * Changing the Mail detail of employee by using the employee id
     * </p>
     * @param employeeId       Id number of employee used to get the particular 
     *                         employee detail
     * @param newMail          New mail Id of employee
     */
    public void changeMail(String employeeId, String newMail) {
        EmployeeView employeeView = new EmployeeView();
        try {
            if (employeeService.changeMail(employeeId, newMail)) {
                employeeView.printMessage(Constant.MAIL_UPDATED_SUCCESS);  
            } else {
                employeeView.printMessage(Constant.MAIL_UPDATED_FAILURE);
            }
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
    }
          
    /**
     * <p>
     * Changing the DOB of employee by using the id
     * </p>
     * @param employeeId      Id number of employee used to get the particular 
     *                        employee detail
     * @param newDob          It replace the existing dob of employee
     */
    public void changeDateOfBirth(String employeeId, String newDob) {
        EmployeeView employeeView = new EmployeeView();
        try {
            if (employeeService.changeDateOfBirth(employeeId, newDob)) {
                employeeView.printMessage(Constant.DOB_UPDATED_SUCCESS);
            } else {
                employeeView.printMessage(Constant.DOB_UPDATED_FAILURE);
            }
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /**
     * <p>
     * Findout the presence of employee using the employee Id
     * </p>
     * @param employeeId       Id number of employee to confirm presence
     * @returns true if the employee Id exists
     */
    public boolean isEmployeePresent(String employeeId) {
        EmployeeView employeeView = new EmployeeView();
        try {
            return employeeService.isEmployeePresent(employeeId);
        } catch (AppException e) {
            employeeView.printMessage(Constant.TRY_AGAIN);
        }
        return false;
    }                   
}
