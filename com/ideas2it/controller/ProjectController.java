package com.ideas2it.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

import com.ideas2it.common.Constant;
import com.ideas2it.exception.AppException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.view.ProjectView;

 /** 
  * <p>
  * Getting all the details from user
  * </p>
  * @author Selvakumar
  * Created 12/07/2017
  */
public class ProjectController {
    private ProjectService projectService = new ProjectServiceImpl();
     
    /**
     * <p>
     * Creating the project detail by passing the project detail like Id,domain
     * to the service class
     * </p>
     * @param projectId                Id number of project used for searching,
     *                                 changing,deleting and displaying the 
     *                                 particular project detail
     * @param projectName              Name of the project
     * @param projectDomain            Domain name of project 
     */
    public void createProject(String projectId, String projectName, 
                                   String projectDomain) {
        ProjectView projectView = new ProjectView();
        try {                           
            if (projectService.createProject(projectId, projectName, 
                                             projectDomain)) {
                projectView.printMessage(Constant.PROJECT_ADDED_SUCCESS);
            } else {
                projectView.printMessage(Constant.INVALID_DATA);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
     
    /**
     * <p>
     * Getting the project detail by using the project id
     * </p>
     * @param projectId                projectId to find the detail of 
     *                                 project
     */    
    public void searchProjectById(String projectId) {
        ProjectView projectView = new ProjectView();
        try {
            Project project = projectService.searchProjectById(projectId);
            if (null == project) {
                projectView.printMessage(Constant.PROJECT_ID_NOT_FOUND);
            } else {
                projectView.printMessage(project.toString());
            }
        } catch (AppException e) {
           projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /** 
     * <p>
     * Removing the project detail by using the project Id
     * </p>
     * @param projectId              Project Id number to get the detail about 
     *                               the project
     */
    public void removeProjectById(String projectId) {
        ProjectView projectView = new ProjectView();
        try {
            if (projectService.removeProjectById(projectId)) {
                projectView.printMessage(Constant.PROJECT_REMOVED_SUCCESS);
            } else {
                projectView.printMessage(Constant.PROJECT_ID_NOT_FOUND);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
            
    /** 
     * <p>
     * Gets all project details
     * </p>
     */
    public void getProjects() {
        ProjectView projectView = new ProjectView();
        try {
            List<Project> projects = projectService.getProjects();
            if (projects.size() > 0) {
                String projectsData = Constant.NEW_LINE;
                for (Project project : projects) {
                    projectsData = projectsData + project.toString();
                }
                projectView.printMessage(projectsData);
            } else {
                projectView.printMessage(Constant.NO_PROJECTS_FOUND);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /** 
     * <p>
     * Changing the Name of project by using the project Id
     * </p>
     * @param projectId      Id of project to get the project information
     * @param projectName    The name which replace the existing project 
     *                       name of project
     */
    public void changeName(String projectId, String newName) {
        ProjectView projectView = new ProjectView();
        try {
            if (projectService.changeName(projectId, newName)) {
                projectView.printMessage(Constant.PROJECT_NAME_UPDATED_SUCCES);
            } else {
                projectView.printMessage(Constant.PROJECT_NAME_UPDATED_FAILURE);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /** 
     * <p>
     * Changing the domain of project by using the project Id
     * </p>
     * @param projectId          Id of project
     * @param projectDomain      New domain name of project
     */
    public void changeDomain(String projectId, String newDomain) {
        ProjectView projectView = new ProjectView();
        try {
            if (projectService.changeDomain(projectId, newDomain)) {
                projectView.printMessage(
                                        Constant.PROJECT_DOMAIN_UPDATED_SUCCES);
            } else {
                projectView.printMessage(
                                       Constant.PROJECT_DOMAIN_UPDATED_FAILURE);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
               
    /** 
     * <p>
     * Assigning project to employee by using the employee Id and project Id
     * </p>
     * @param projectId                 Id number of project which is to be
     *                                  assigned on employee
     * @param employeeId                Id number of employee to get the  
     *                                  employee profile
     * @param employeeDesignation       Designation of employee
     * @param employeeMailId            MailId of employee and it is unique
     * @param employeeName              Name of employee
     * @param employeeDob               Birth date of employee
     */
    public void assignProjectToEmployee(String projectId, String employeeId,
                                             String employeeDesignation, 
                                             String employeeMailId,
                                             String employeeName,
                                             String employeeDob) {
        EmployeeService employeeService = new EmployeeServiceImpl();
        ProjectView projectView = new ProjectView();
        try {
            if (employeeService.createEmployee(employeeId, employeeDesignation,
                                                   employeeMailId, employeeName,
                                                   employeeDob)) {
                projectService.assignProjectToEmployee(projectId, employeeId);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /** 
     * <p>
     * Removing employee from project by using the employee Id and project Id
     * </p>
     * @param projectId          Id number of project which is to be assigned on 
     *                           employee
     * @param employeeId         Id number of employee to get the employee 
     *                           profile
     */
    public void removeEmployeeFromProject(String projectId, String employeeId) {
        ProjectView projectView = new ProjectView();
        try {
            if (!projectService.removeEmployeeFromProject(projectId, 
                                                               employeeId)) {
                projectView.printMessage(Constant.EMPLOYEE_ID_NOT_FOUND);
            } else {
                projectView.printMessage(Constant.REMOVED_SUCCESS);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }  
    }
     
    /** 
     * <p>
     * Gets the employees detail assigned to the particular project by using 
     * the project Id
     * </p>
     * @param projectId          Id number of project to get the detail about
     *                           project
     */    
    public void getEmployeesInProject(String projectId) {
        ProjectView projectView = new ProjectView();
        try {
            List<Employee> employees = 
                                projectService.getEmployeesInProject(projectId);
            if ((null != employees) && (employees.size() > 0)) {
                String employeesData = Constant.NEW_LINE;
                for (Employee employee : employees) {
                    employeesData = employeesData + employee.toString();
                }
                projectView.printMessage(employeesData);
            } else {
                projectView.printMessage(Constant.NO_EMPLOYEE_ASSIGNED);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /** 
     * <p>
     * Gets the employees detail who are not assigned to any project
     * </p>
     */    
    public void getEmployeesNotInProject() {
        ProjectView projectView = new ProjectView();
        try {
            List<List<String>> employees = 
                                 projectService.getEmployeesNotInProject();
            if (null != employees) {
                for (List<String> employee : employees) {
                    projectView.printMessage(Constant.ID_EMPLOYEE 
                                              + employee.get(0) 
                                              + Constant.NEW_LINE
                                              + Constant.NAME_EMPLOYEE 
                                              + employee.get(1) 
                                              + Constant.NEW_LINE
                                              + Constant.MAIL_EMPLOYEE 
                                              + employee.get(2)
                                              + Constant.NEW_LINE);
                }
            } else {
                projectView.printMessage(Constant.NO_EMPLOYEE_FREE);
            }
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        }
    }
    
    /** 
     * <p>
     * Find the presence of project corresponding to the id
     * </p>
     * @param projectId          Id number of project
     * @return true if the project Id already exists
     */
    public boolean isProjectPresent(String projectId) {
        ProjectView projectView = new ProjectView();
        try {
            return projectService.isProjectPresent(projectId);
        } catch (AppException e) {
            projectView.printMessage(Constant.TRY_AGAIN);
        } 
        return false;
    }                     
}
