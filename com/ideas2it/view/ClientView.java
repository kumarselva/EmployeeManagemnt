package com.ideas2it.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.common.Constant;
import com.ideas2it.controller.ClientController;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;

/**
 * <p>
 * Performs the the particular operation based on the user opinion like 
 * creating,searching,changing,removing and displaying the client detail
 * </p> 
 * @author Selvakumar
 * Created 12/07/2017
 */
public class ClientView {
    private Scanner scan = new Scanner(System.in);
    private ClientController clientController = 
                                                      new ClientController();   
    /**
     * <p>
     * Performs the certain operation on client whatever the user wants to
     * perform from the shown detail like Creating creating,searching,changing,
     * removing and displaying the client detail
     * </p>
     */
    public void manageClient() {
        boolean flag = true;
        try {
            do {
                System.out.println(Constant.USER_OPINION_CLIENT);
                switch (scan.nextInt()) {
                    case 1:
                        createClient(); 
                        break;
                    case 2:
                        searchClientById();
                        break;      
                    case 3:
                        removeClientById();
                        break;
                    case 4:
                        showClients();
                        break;
                    case 5:
                        changeClientInfo();
                        break;
                    case 6:
                        assignProjectToClient();
                        break;
                    default:
                        flag = false;
                        break;
                }
            } while (flag);       
        } catch(InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        }
        scan.nextLine();
    }
    
    /**
     * <p>
     * Displays the message which is passed as a parameter
     * </p>
     * @param message         The information which is to be displayed
     */
    public void printMessage(String message) {
        System.out.println(message);
    }                    

    /**
     * <p>
     * Creating an client by getting their informations like name,mailId
     * and their organisation name
     * </p>
     */
    private void createClient() {
        System.out.println(Constant.CLIENT_ID);
        String clientId = scan.next();
        System.out.println(Constant.CLIENT_NAME);
        String clientName = scan.next();
        System.out.println(Constant.CLIENT_MAIL);
        String clientMail = scan.next();
        System.out.println(Constant.CLIENT_ORGANISATION);
        String clientOrganisation = scan.next();
        if (clientController.createClient(clientId, clientName, clientMail, 
                                             clientOrganisation)) {
            createAddress("Create Address", clientId);
            printMessage(Constant.SUCCESS_CREATE);
        } else {
            printMessage(Constant.FAILURE_CREATE);
        }
    }
    
    /**
     * <p> 
     * Searching the client detail by using client ID number
     * </p>
     */   
    private void searchClientById() {
        System.out.println(Constant.SEARCH_CLIENT);
        String clientId = scan.next();
        Client client = clientController.getClientById(clientId);
        if (null == client) {
            printMessage(Constant.CLIENT_ID_NOT_FOUND);
        } else {
            System.out.println(client);
        }
        searchAddressById(clientId);
    } 
 
    /**
     * <p> 
     * Searching the client detail by using client ID number
     * </p>
     */   
    private void searchAddressById(String clientId) {
        List<Address> addresses = clientController.getAddressById(clientId);
        if (null == addresses) {
            printMessage(Constant.CLIENT_ID_NOT_FOUND);
        } else {
            for (Address address : addresses) {
                System.out.println(address);
            }
        }
    } 
    
    /**
     * <p>
     * Delete the particular client detail by using the client ID number
     * </p>
     */
    private void removeClientById() {
        System.out.println(Constant.REMOVE_CLIENT);
        String clientId = scan.next();
        if (clientController.removeClientById(clientId)) {
            printMessage(Constant.DATA_REMOVED_SUCCESS);
        } else {
            printMessage(Constant.CLIENT_ID_NOT_FOUND);
        }
    }
            
    /**
     * <p>
     * Displays all the emloyees detail which includes name,id,mailId,
     * Designation,DOB
     * </p> 
     */
    private void showClients() {
        List<Client> clients = clientController.getClients();
        if (null != clients) {
            for (Client client : clients) {
                System.out.println(client);
            }
        } else {
            System.out.println(Constant.NO_CLIENTS_FOUND);
        }
    }
    
    /**
     * <p>
     * Updating the client detail by using the client ID. If the ID exists,
     * user can perform changing operation on name,mail and organisation name.
     * If the ID doesn't exist, user can't update the information.
     * </p>
     */
    private void changeClientInfo() {
        try {   
            System.out.println(Constant.CLIENT_ID);
            String clientId = scan.next();
            if (clientController.isClientPresent(clientId)) {  
                boolean flag = true;
                do {
                    System.out.println(Constant.UPDATE_CLIENT);
                    switch (scan.nextInt()) {
                        case 1:
                            changeClientInformation(clientId);
                            break;
                         case 2:
                            createAddress("Change Address", clientId);
                            break;
                        default :
                            flag = false;
                            break;
                    }
                } while (flag); 
            } else {
                System.out.println(Constant.CLIENT_ID_NOT_FOUND);
            }
        } catch (InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        }   
    }

    /**
     * <p>
     * Assigning the project to the corresponding client detail by using the 
     * client ID.
     * </p>
     */
    private void assignProjectToClient() {
        System.out.println(Constant.CLIENT_ID);
        String clientId = scan.next();
        System.out.println(Constant.PROJECT_ID);
        String projectId = scan.next();
        System.out.println(Constant.PROJECT_NAME);
        String projectName = scan.next();
        System.out.println(Constant.PROJECT_DOMAIN);
        String projectDomain = scan.next();
        clientController.assignProjectToClient(clientId, projectId, 
                                                   projectName, projectDomain);
    }
    
    /**
     * <p>
     * Changes the name of client corresponding to the client ID
     * </p>
     * @param clientId      Id number of client used for the name changing
     *                        purpose
     */
    private void changeClientInformation(String clientId) {
        System.out.println(Constant.CLIENT_NAME);
        String clientName = scan.next();
        System.out.println(Constant.CLIENT_MAIL);
        String clientMail = scan.next();
        System.out.println(Constant.CLIENT_ORGANISATION);
        String clientOrganisation = scan.next();
        clientController.changeClientInformation(clientId,clientName,
                                                    clientMail, 
                                                    clientOrganisation);
    }
    
    /**
     * <p>
     * Creating an client address by getting their informations like doorNo,
     * streeet name, landmark, district, state, country, postal code..
     * Changing an client address by getting their informations like doorNo,
     * streeet name, landmark, district, state, country, postal code..
     * </p>
     */
    private void createAddress(String operation, String clientId) {
        System.out.println(Constant.DOOR_NO);
        String doorNo = scan.next();
        System.out.println(Constant.STREET_NAME);
        String street = scan.next();
        System.out.println(Constant.LAND_MARK);
        String landMark = scan.next();
        System.out.println(Constant.CITY);
        String city = scan.next();
        System.out.println(Constant.POSTAL_CODE);
        String postalCode = scan.next();
        System.out.println(Constant.DISTRICT);
        String district = scan.next();
        System.out.println(Constant.STATE);
        String state = scan.next();
        System.out.println(Constant.COUNTRY);
        String country = scan.next();
        switch (operation) {
            case "Create Address":
                clientController.createAddress(doorNo, street, landMark, city,
                                                   postalCode, district, state, 
                                                   country, clientId);
                break;
            case "Change Address":
                clientController.changeAddress(doorNo,street, landMark,
                                                   city, postalCode, 
                                                   district, state, country, 
                                                   clientId);
                break;
            default:
                break;
        }
    }
}
