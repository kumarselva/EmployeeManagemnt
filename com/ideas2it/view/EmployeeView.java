package com.ideas2it.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.common.Constant;
import com.ideas2it.controller.EmployeeController;
import com.ideas2it.model.Address;
import com.ideas2it.model.Employee;

/**
 * <p>
 * Performs the the particular operation based on the user opinion like 
 * creating,searching,changing,removing and displaying the employee detail
 * </p> 
 * @author Selvakumar
 * Created 12/07/2017
 */
public class EmployeeView {
    private Scanner scan = new Scanner(System.in);
    private EmployeeController employeeController = new EmployeeController();   
    /**
     * <p>
     * Performs the certain operation on employee whatever the user wants to
     * perform from the shown detail like Creating creating,searching,changing,
     * removing and displaying the employee detail
     * </p>
     */
    public void manageEmployee() {
        boolean flag = true;
        try {
            do {
                System.out.println(Constant.USER_OPINION_EMPLOYEE);
                switch (scan.nextInt()) {
                    case 1:
                        createEmployee(); 
                        break;
                    case 2:
                        searchEmployeeById();
                        break;      
                    case 3:
                        removeEmployeeById();
                        break;
                    case 4:
                        showEmployees();
                        break;
                    case 5:
                        updateEmployee();
                        break;
                    default:
                        flag = false;
                        break;
                }
            } while (flag);       
        } catch(InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        }
        scan.nextLine();
    }
    
    /**
     * <p>
     * Displays the message which is passed as a parameter
     * </p>
     * @param message         The information which is to be displayed
     */
    public void printMessage(String message) {
        System.out.println(message);
    }                    

    /**
     * <p>
     * Creating an employee by getting their informations like name,Id,mailId
     * and their date of birth
     * </p>
     */
    private void createEmployee() {
        System.out.println(Constant.EMPLOYEE_ID);
        String employeeId = scan.next();
        System.out.println(Constant.EMPLOYEE_DESIGNATION);
        String employeeDesignation = scan.next();
        System.out.println(Constant.EMPLOYEE_MAIL);
        String employeeMailId = scan.next();
        System.out.println(Constant.EMPLOYEE_NAME);
        String employeeName = scan.next();
        System.out.println(Constant.EMPLOYEE_DOB);
        String employeeDob = scan.next();
        if (employeeController.createEmployee(employeeId, employeeDesignation, 
                                   employeeMailId, employeeName,employeeDob)) {
            createAddress("Create Address", employeeId);
        } else {
            printMessage(Constant.FAILURE_CREATE);
        }
    }
    
    /**
     * <p> 
     * Searching the employee detail by using employee ID number
     * </p>
     */   
    private void searchEmployeeById() {
        System.out.println(Constant.SEARCH_EMPLOYEE);
        String employeeId = scan.next();
        Employee employee = employeeController.getEmployeeById(employeeId);
        if (null == employee) {
            printMessage(Constant.EMPLOYEE_ID_NOT_FOUND);
        } else {
            System.out.println(employee);
        }
        searchAddressById(employeeId);
    } 
 
    /**
     * <p> 
     * Searching the employee detail by using employee ID number
     * </p>
     */   
    private void searchAddressById(String employeeId) {
        List<Address> addresses = employeeController.getAddressById(employeeId);
        if (null == addresses) {
            printMessage(Constant.EMPLOYEE_ID_NOT_FOUND);
        } else {
            for (Address address : addresses) {
                System.out.println(address);
            }
        }
    } 
    
    /**
     * <p>
     * Delete the particular employee detail by using the employee ID number
     * </p>
     */
    private void removeEmployeeById() {
        System.out.println(Constant.REMOVE_EMPLOYEE);
        String employeeId = scan.next();
        if (employeeController.removeEmployeeById(employeeId)) {
            printMessage(Constant.DATA_REMOVED_SUCCESS);
        } else {
            printMessage(Constant.EMPLOYEE_ID_NOT_FOUND);
        }
    }
            
    /**
     * <p>
     * Displays all the emloyees detail which includes name,id,mailId,
     * Designation,DOB
     * </p> 
     */
    private void showEmployees() {
        List<Employee> employees = employeeController.getEmployees();
        if (null != employees) {
            for (Employee employee : employees) {
                System.out.println(employee);
            }
        } else {
            System.out.println(Constant.NO_EMPLOYEES_FOUND);
        }
    }
    
    /**
     * <p>
     * Updating the employee detail by using the employee ID. If the ID exists,
     * user can perform changing operation on name,designation,mail and DOB.
     * If the ID doesn't exist, user can't update the information.
     * </p>
     */
    private void updateEmployee() {
        try {   
            System.out.println(Constant.EMPLOYEE_ID);
            String employeeId = scan.next();
            if (employeeController.isEmployeePresent(employeeId)) {  
                boolean flag = true;
                do {
                    System.out.println(Constant.UPDATE_EMPLOYEE);
                    switch (scan.nextInt()) {
                        case 1:
                            changeName(employeeId);
                            break;
                        case 2:
                            changeDesignation(employeeId);
                            break;
                        case 3:
                            changeMailId(employeeId);
                            break;
                        case 4:
                            changeDateOfBirth(employeeId);
                            break;
                        case 5:
                            createAddress("Change Address", employeeId);
                            break;
                        default :
                            flag = false;
                            break;
                    }
                } while (flag); 
            } else {
                System.out.println(Constant.EMPLOYEE_ID_NOT_FOUND);
            }
        } catch (InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        }   
    }
    
    /**
     * <p>
     * Changes the name of employee corresponding to the employee ID
     * </p>
     * @param employeeId      Id number of employee used for the name changing
     *                        purpose
     */
    private void changeName(String employeeId) {
        System.out.println(Constant.NEW_EMPLOYEE_NAME);
        String newName = scan.next(); 
        employeeController.changeName(employeeId, newName);
    }
    
    /**
     * <p>
     * Changes the designation of employee corresponding to the employee ID
     * </p>
     * @param employeeId      Id number of employee used for changing the
     *                        designation of employee
     */
    private void changeDesignation(String employeeId) {
        System.out.println(Constant.NEW_EMPLOYEE_DESIGNATION);
        String newDesignation = scan.next();
        employeeController.changeDesignation(employeeId, newDesignation);
    }
    
    /**
     * <p>
     * Changes the mail ID of employee corresponding to the employee ID
     * </p>
     * @param employeeId      Id number of employee used for changing the
     *                        mail information of employee
     */ 
    private void changeMailId(String employeeId) {
            System.out.println(Constant.NEW_EMPLOYEE_MAIL);
            String newMail = scan.next();
            employeeController.changeMail(employeeId, newMail);
    }

    /**
     * <p>
     * Changes the employee DOB by using the ID number
     * </p>
     * @param employeeId      Id number of employee used for changing the 
     *                        DOB of employee
     */
    private void changeDateOfBirth(String employeeId) {
        System.out.println(Constant.NEW_EMPLOYEE_DOB);
        String changeDob = scan.next(); 
        employeeController.changeDateOfBirth(employeeId, changeDob);
    }

    /**
     * <p>
     * </p>
     */
    
    /**
     * <p>
     * Creating an employee address by getting their informations like doorNo,
     * streeet name, landmark, district, state, country, postal code..
     * Changing an employee address by getting their informations like doorNo,
     * streeet name, landmark, district, state, country, postal code..
     * </p>
     */
    private void createAddress(String operation, String employeeId) {
        System.out.println(Constant.DOOR_NO);
        String doorNo = scan.next();
        System.out.println(Constant.STREET_NAME);
        String street = scan.next();
        System.out.println(Constant.LAND_MARK);
        String landMark = scan.next();
        System.out.println(Constant.CITY);
        String city = scan.next();
        System.out.println(Constant.POSTAL_CODE);
        String postalCode = scan.next();
        System.out.println(Constant.DISTRICT);
        String district = scan.next();
        System.out.println(Constant.STATE);
        String state = scan.next();
        System.out.println(Constant.COUNTRY);
        String country = scan.next();
        switch (operation) {
            case "Create Address":
                employeeController.createAddress(doorNo,
                                        street, landMark, city, postalCode, 
                                        district, state, country, employeeId);
                break;
            case "Change Address":
                employeeController.changeAddress (doorNo,street, landMark,
                                                   city, postalCode, 
                                                   district, state, country, 
                                                   employeeId);
                break;
            default:
                break;
        }
    }
}
