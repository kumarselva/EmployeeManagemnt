package com.ideas2it.util;

import java.util.Calendar;

/**
 * <p>
 * Caluates the age of a person
 * </p>
 */
public class DateUtil {
    public static int calculateAge(String dateOfBirth) {
        Calendar today = Calendar.getInstance();
        int currentDay = today.get(Calendar.DAY_OF_MONTH);
        int currentMonth = today.get(Calendar.MONTH);
        int currentYear = today.get(Calendar.YEAR);
        String birth[] = dateOfBirth.split("-");
        int dob[] = new int[3];
        for (int i = 0; i<3; i++) {
            dob[i] = Integer.parseInt(birth[i]);
        }
        int age = currentYear - dob[2];
        if (dob[1] > currentMonth) {
            age--;
        } else if (currentMonth == dob[1]) {
            if (dob[0] > currentDay) {
                age--;
            }
        }
        return age;
    }
}
