package com.ideas2it.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * This class is used for validating purpose
 * </p>
 */
public class CommonUtil {

    /**
     * @param employeeId            Id number of employee       
     * 4 digit number Ex:1234
     */
    public static boolean isValidId(String id) {
        final String idPattern = "[0-9]*";
        return isValidData(idPattern, id);
    }

    /**
     * @param employeeId            Id number of employee       
     * 4 digit number Ex:1234
     */
    public static boolean isValidEmployeeId(String id) {
        final String idPattern = "[E][-][0-9]*";
        return isValidData(idPattern, id);
    }

    /**
     * @param employeeId            Id number of employee       
     * 4 digit number Ex:1234
     */
    public static boolean isValidClientId(String id) {
        final String idPattern = "[C][-][0-9]*";
        return isValidData(idPattern, id);
    }
    
    /** 
     * @param employeeDesignation   Designation of the employee
     * Valid : Software Developer, Police
     * Invalid : Software developer,po12
     */
    public static boolean isValidName(String name) {                         
        final String namePattern = "[A-Z]{1}[a-z]*";        
        return isValidData(namePattern, name);
    }
    
    /** 
     * @param employeeMailId        MailId of employee
     * Valid : ideas2it@gmail.com
     * Invalid:Ideas2it15.com
     */
    public static boolean isValidMailId(String mailId) {
        final String mailPattern = "[a-z]*[0-9\\.]*[@]{1}[a-z]*[^0-9][\\.]{1}"
                                    + "[a-z]{3}";
        return isValidData(mailPattern, mailId);
    }
    
    /** 
     * @param dob        Date of birth of employee
     * Valid : 04-08-1996
     * Invalid:09/08/1264, 1996-04-08
     */
    public static boolean isValidDateOfBirth(String dob) {
        final String DATE_FORMAT = "dd-MM-yyyy";
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(dob);
            return true;
        } catch (ParseException e) {
            return false;
        }        
    }

    /**
     * Compare two strings and returns true if both the strings matches
     */
    public static boolean isValidData(String actualPattern, 
                                                       String dataFromUser) {
        return Pattern.matches(actualPattern, dataFromUser);
    }
}

