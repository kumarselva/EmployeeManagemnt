package com.ideas2it.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {
    private static Connection connection;
    private static DataBaseConnection dbConnection ;
    private  String url = "jdbc:mysql://localhost/"
                                 + "IDEAS2IT?autoReconnect=true&useSSL=false";
    private  String user = "root";
    private  String password = "root";
     
    /**
     * <p>
     * Connects to the database and returns the connected object
     * </p>
     * @return      a connection object that holds connection to database
     */    
    public Connection getConnection() throws SQLException {        
        try {
            connection = DriverManager.getConnection(url, user, password);
            return connection;
        } catch (SQLException exception) {
            throw exception;
        }
    }
    
    /**
     * <p>
     * Creation of singleton object
     * </p>
     * @return Database connection object
     */
    public static synchronized DataBaseConnection getInstance() {
        if ( null == dbConnection) {
            synchronized (DataBaseConnection.class) {
                if (null == dbConnection) {
                    dbConnection = new DataBaseConnection();
                }
            }
        }
        return dbConnection;
    }

    /**
     * <p>
     * Closes the connection with database
     * </p>
     */     
    public static void closeConnection() throws SQLException{
        try {
            connection.close();
        } catch (SQLException exception) {
            throw exception;
        }
    }

    /**
     * <p>
     * Empty constructor
     * </p>
     */     
    private DataBaseConnection() {
    }
}
