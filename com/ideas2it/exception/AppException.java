package com.ideas2it.exception;

/**
 * <p>
 * Handles the runtime exception
 * </p>
 */
public class AppException extends RuntimeException {

    /**
     * <p>
     * Passes null to the parent class constructor
     * </p>
     */
    public AppException() {
        super ();
    }

    /**
     * <p>
     * Passes information about the exception to the parent class constructor
     * </p>
     * @param message                      Information about the exception
     */    
    public AppException(String message) {
        super (message);
    }

    /**
     * <p>
     * Passes causes about the exception to the parent class constructor
     * </p>
     * @param cause                        Causes about the exception
     */    
    public AppException(Throwable cause) {
        super (cause);
    }
    
    /**
     * <p>
     * Passes information causes about the exception to the parent class 
     * constructor
     * </p>
     * @param message                      Information about the exception
     * @param cause                        Causes about the exception
     */
    public AppException(String message, Throwable cause) {
        super (message, cause);
    }
}
