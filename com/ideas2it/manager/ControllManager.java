package com.ideas2it.manager;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.ideas2it.common.Constant;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.ProjectView;
import com.ideas2it.view.ClientView;

class ControllManager {
     /**
     * Gets the information from the user to perform the particular operation
     */
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        try {
            do {
                System.out.println(Constant.USER_OPINION);
                int operation = scan.nextInt();
                switch(operation) {
                    case 1:
                        EmployeeView employeeView = new EmployeeView();
                        employeeView.manageEmployee();
                        break;
                    case 2:
                        ProjectView projectView = new ProjectView();
                        projectView.manageProject();
                        break;
                    case 3:
                        ClientView clientView = new ClientView();
                        clientView.manageClient();
                        break;
                    default :
                        System.exit(0);
                        break;
                } 
            } while (true);
        } catch(InputMismatchException e) {
            System.out.println(Constant.INVALID_INPUT);
        } scan.nextLine();
    }
}
            
