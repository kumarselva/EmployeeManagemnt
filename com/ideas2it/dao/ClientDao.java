package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.AppException;
import com.ideas2it.model.Client;
 
/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * client
 * </p>
 * @author Selvakumar
 */ 
public interface ClientDao { 

    /**
     * <p>
     * Insert the client detail like name, dob, mail, designation
     * </p>
     * @param client           Object of the Client class that has the 
     *                         following parameters like client name, mail, 
     *                         organisation
     */
    boolean insertClient(Client client) throws AppException;         
            
    /**
     * <p>
     * Getting the client profile corresponding to the id
     * </p>
     * @param clientId            Id number of client used to get the 
     *                              particular client detail
     */
    Client getClient(String clientId) throws AppException; 
           
    /**
     * <p>
     * Removing the client detail corresponding to the id
     * </p>
     * @param clientId             Id number of client is an primary key.
     *                             So by using this key we can easily remove the 
     *                             client
     */
    void deleteClient(String clientId) throws AppException; 

    /**
     * <p>
     * Updating the client detail like name, mail, dob, designation by using 
     * the id
     * </p>
     * @param client           Object of the Client class that has the 
     *                         following parameters like client name, mail, 
     *                         organisation
     */
    void updateClientInfo(Client client) throws AppException; 
          
    /**
     * </p>
     * Retrieving all clients detail
     * </p>
     */ 
    List<Client> retrieveClients() throws AppException;

    /**
     * <p>
     * Assigning the project detail to the particular client
     * </p>
     * @param clientId                Id number of client used for 
     *                                searching, deleting and displaying the 
     *                                particular client detail
     * @param projectId               Id number of project used for searching,
     *                                deleting,displaying the particular project
     *                                detail
     */        
    void assignProjectToClient(String clientId, String projectId) 
                                         throws AppException;       
}

