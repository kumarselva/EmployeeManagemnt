package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.AppException;
import com.ideas2it.model.Employee;
 
/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * employee
 * </p>
 * @author Selvakumar
 */ 
public interface EmployeeDao { 

    /**
     * <p>
     * Insert the employee detail like name, dob, mail, designation
     * </p>
     * @param employee         Object of the Employee class that has the 
     *                         following parameters like name, dob, mail, 
     *                         designation
     */
    void insertEmployee(Employee employee) throws AppException;         
            
    /**
     * <p>
     * Getting the employee profile corresponding to the id
     * </p>
     * @param employeeId            Id number of employee used to get the 
     *                              particular employee detail
     */
    Employee getEmployee(String employeeId) throws AppException; 
           
    /**
     * <p>
     * Removing the employee detail corresponding to the id
     * </p>
     * @param employeeId            Id number of employee is an primary key.
     *                              So using this key we can easily remove the 
     *                              employee
     */
    void deleteEmployee(String employeeId) throws AppException; 
    
    /**
     * <p>
     * Change employee name by using the employeeId
     * </p>
     * @param employeeId                Employee Id number to get the 
                                        details about employee
     * @param newName                   The name which replace the existing name
     *                                  of the employee
     */
    void updateEmployeeName(String employeeId, String newName)
                                     throws AppException; 

    /**
     * <p>
     * Updating the employee Designation corresponding to the id
     * </p>
     * @param employeeId            Id number of employee to get the employee 
     *                              profile
     * @param newDesignation        New designation of employee
     */
    void updateEmployeeDesignation(String employeeId, 
                                             String newDesignation)
                                             throws AppException; 
    
    /**
     * <p>
     * Updating the employee mail corresponding to the id
     * </p>
     * @param employeeId            Id number of employee used to get the 
     *                              employee profile
     * @param newMail               New Mail Id of employee
     */
    void updateEmployeeMail(String employeeId, String newMail) 
                                   throws AppException; 
    
    /**
     * <p>
     * Updating the employee DOB corresponding to the id
     * </p>
     * @param employeeId            Id number of employee to get the profile
     * @param newDob                It replace the existing dob of employee
     */
    void updateEmployeeDob(String employeeId, String newDob) 
                                     throws AppException; 
            
    /**
     * <p>
     * Updating the employee detail like name, mail, dob, designation by using 
     * the id
     * </p>
     * @tableColumnName             Column name of table
     * @param employeeId            Id number of employee to get the profile of 
     *                              employee
     * @param dataToBeUpdated       Employee data which is to be changed
     */
    void updateEmployee(String tableColumnName, 
                                String employeeId, String dataToBeUpdated)
                                throws AppException; 
          
    /**
     * </p>
     * Retrieving all employees detail by adding employees detail into list
     * </p>
     */ 
    List<Employee> retrieveEmployees() throws AppException;       
}

