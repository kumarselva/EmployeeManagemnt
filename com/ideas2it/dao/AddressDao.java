package com.ideas2it.dao;


import java.util.List;

import com.ideas2it.exception.AppException;;
import com.ideas2it.model.Address;
 
/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * particular person's address
 * </p>
 * @author Selvakumar
 */ 
public interface AddressDao { 

    /**
     * <p>
     * Insert the address detail like door number ,street name, city, land mark
     * state name, country name
     * </p>
     * @param employee         Object of the address class that has the 
     *                         following parameters like door number, street,
     *                         city, landMark, postal code, country, state 
     */
    boolean insertAddress(Address address, String Id) throws AppException;
             
    /**
     * <p>
     * Change the address detail like name, dob, mail, designation
     * </p>
     * @param employee         Object of the address class that has the 
     *                         following parameters like door number, street,
     *                         city, landMark, postal code, country, state 
     */
    boolean updateAddress(Address address, String Id) throws AppException;
    
    /**
     * <p>
     * Delete the address detail like door number ,street name, city, land mark
     * state name, country name
     * </p>
     * @param employee         Object of the address class that has the 
     *                         following parameters like door number, street,
     *                         city, landMark, postal code, country, state 
     */
    boolean deleteAddress(String Id) throws AppException;         
    
    /**
     * <p>
     * Getting the address corresponding to the id
     * </p>
     * @param Id               Id number of person used to get the 
     *                         particular person address
     */
    List<Address> getAddress(String Id) throws AppException;     
           
}
