package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.AppException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
 
/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * project
 * </p>
 * @author Selvakumar
 */
public interface ProjectDao {
        
    /**
     * <p>
     * Insert the project detail like project Id,name,domain name
     * </p>
     * @param project          Object of the Project class that has the 
     *                         following parameters like project name,Id, 
     *                         domain Name
     */
    void insertProject(Project project) throws AppException;
         
    /**
     * <p>
     * Getting the project detail corresponding to the project Id 
     * </p>
     * @param projectId            Id number of project to get the project 
     *                             profile
     */
    Project getProject(String projectId) throws AppException;
     
    /**
     * <p>
     * Removing the project detail corresponding to the project Id
     * </p>
     * @param projectId            Id number of project to get the project 
     *                             detail
     */
    void deleteProject(String projectId) throws AppException;
        
    /**
     * <p>
     * Assigning the project to the employee by using employee Id and project Id
     * </p>
     * @param projectId            Id number of project which is to be assigned
     *                             on employee
     * @param employeeId           Id number of employee to get the employee 
     *                             profile
     */
    void assignProjectToEmployee(String projectId, String employeeId) 
                                    throws AppException;        
     
    /**
     * <p>
     * Removing the employee from particular project
     * </p>
     * @param projectId            Id number of project which is to be removed
     *                             from employee profile
     * @param employeeId           Id number of Employee to get the profile
     */
    void deleteEmployeeFromProject(String projectId, String employeeId)
                                      throws AppException;
    
 
    /**
     * <p>
     * Updating the project detail corresponding to the Id
     * </p>
     * @param projectId            Id number of project to get the project 
     *                             detail
     * @param newName              It replace the existing name of project
     */
    void updateProjectName(String projectId, String newName)
                                                           throws AppException;
        
    /**
     * <p>
     * Updating the project Designation corresponding to the proId
     * </p>
     * @param projectId            Id number of project to get the project 
     *                             detail
     * @param newDomain            New Domain Name of project
     */
    void updateProjectDomain(String projectId, String newDomain)
                                   throws AppException;       
                
    /**
     * <p>
     * Updating the project detail corresponding to the proId using queries
     * </p>
     * @tableColumnName             Column name of table in database
     * @param projectId             Id number of project to get the project 
     *                              detail
     * @param dataToBeUpdated       Project data which is to be changed
     */
    void updateProject(String tableColumnName, String projectId, 
                               String dataToBeUpdated) throws AppException;
    
    /**
     * <p>
     * Retrieving the projects detail
     * </p> 
     * @returns the projects detail by adding the project detail into the list 
     */
    List<Project> retrieveProjects() throws AppException;
           
    /**
     * <p>
     * Getting employees detail corresponding to the project Id
     * </p>
     * from database
     * @param projectId                Id number of project to get the employees
     *                                 detail coresponding to the project Id 
     */
    List<Employee> getEmployeesInProject(String projectId) throws AppException;
           
    /**
     * <p>
     * Getting employees detail who are not involved in any project. 
     * </p>
     * @returns list of employees who are not in any project
     */
    List<List<String>> getEmployeesNotInProject() throws AppException;
}

