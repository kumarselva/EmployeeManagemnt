package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.common.Constant;
import com.ideas2it.connection.DataBaseConnection;
import com.ideas2it.dao.EmployeeDao;
import com.ideas2it.exception.AppException;
import com.ideas2it.logger.LoggerInfo;
import com.ideas2it.model.Employee;

/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * employee
 * </p>
 * @author Selvakumar
 */ 
public class EmployeeDaoImpl implements EmployeeDao {
    private final String empId = "EMPLOYEE_ID";
    private final String empName = "EMPLOYEE_NAME";
    private final String empMail = "EMPLOYEE_MAIL";
    private final String empDob = "EMPLOYEE_DOB";
    private final String empDesignation = "EMPLOYEE_DESIGNATION";    
    private Connection connection;

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #void insertEmployee(Employee employee)
     */
    public void insertEmployee(Employee employee) throws AppException {        
        try {
            getConnection();
            if (null != connection) {
                String sql = "INSERT INTO EMPLOYEE(" + empId + "," + empName 
                                    + "," + empDob + "," + empDesignation + ","
                                    + empMail + ")" + "VALUES(?,?,?,?,?)";
                PreparedStatement statement =  connection.prepareStatement(sql);
                statement.setString(1,employee.getId());
                statement.setString(2,employee.getName());
                statement.setString(3,employee.getDateOfBirth());           
                statement.setString(4,employee.getDesignation());
                statement.setString(5,employee.getMailId());
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception occured "
                                   + "while inserting data " + employee, 
                                   exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #Employee getEmployee(String employeeId)
     */    
     public Employee getEmployee(String employeeId) throws AppException {
        try {
            getConnection();
            Employee employee = null;
            if (null != connection) {
                PreparedStatement statement = 
                       connection.prepareStatement("SELECT " + empId + "," 
                                                  + empName + "," + empDob + "," 
                                                  + empDesignation + "," 
                                                  + empMail + " FROM "
                                                  + "EMPLOYEE WHERE " + empId
                                                  + " = ?");
                statement.setString(1, employeeId);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    employee = new Employee(resultSet.getString(empId),  
                                            resultSet.getString(empDesignation),
                                            resultSet.getString(empMail),
                                            resultSet.getString(empName),
                                            resultSet.getString(empDob));
                }
                resultSet.close();
                statement.close();
                return employee;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception "
                                   + "occured while getting employee data by"
                                   + "using id = " + employeeId, exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }     
        return null;
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #void deleteEmployee(String employeeId)
     */    
    public void deleteEmployee(String employeeId) throws AppException {
       try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                           connection.prepareStatement("DELETE FROM EMPLOYEE "
                                                           + "WHERE " + empId
                                                           + " = ?");
                statement.setString(1, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception "
                                    + "occured while deleting employee using "
                                    + "employee Id = "  + employeeId,
                                    exception);                                   
               exception.printStackTrace();
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }    
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #void updateEmployeeName(String employeeId, String newName)
     */
    public void updateEmployeeName(String employeeId, String newName)
                                     throws AppException {
        updateEmployee(empName, employeeId, newName);
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #void updateEmployeeDesignation(String employeeId, 
     *                                        String newDesignation)
     */    
    public void updateEmployeeDesignation(String employeeId, 
                                             String newDesignation)
                                             throws AppException {
        updateEmployee(empDesignation, employeeId, newDesignation);
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * # public void updateEmployeeMail(String employeeId, String newMail)
     */    
   public void updateEmployeeMail(String employeeId, String newMail) 
                                   throws AppException {
        updateEmployee(empMail, employeeId, newMail);
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #void updateEmployeeDob(String employeeId, String newDob)
     */
    public void updateEmployeeDob(String employeeId, String newDob) 
                                     throws AppException {
        updateEmployee(empDob, employeeId, newDob);   
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #void updateEmployee(String tableColumnName, 
     *                              String employeeId, String dataToBeUpdated)
     */       
    public void updateEmployee(String tableColumnName, 
                                String employeeId, String dataToBeUpdated)
                                throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement =  
                                  connection.prepareStatement("UPDATE "
                                                              + "EMPLOYEE SET " 
                                                              + "? = ? "
                                                              +"WHERE "
                                                              + empId 
                                                              + "= ?");  
                statement.setString(1,tableColumnName);
                statement.setString(2, dataToBeUpdated);
                statement.setString(3, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception "
                            + "occured while updating " + dataToBeUpdated
                            + " in column " + tableColumnName  
                            + " by using the employee Id = " + employeeId,
                            exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }    
    }

    /**
     * @com.ideas2it.dao.EmployeeDao
     * #List<Employee> retrieveEmployees()
     */    
    public List<Employee> retrieveEmployees() throws AppException {
        try {
            getConnection();
            if (null != connection) {
                List<Employee> employees = new ArrayList<Employee>();
                PreparedStatement statement = 
                             connection.prepareStatement("SELECT " 
                                                             + empId + "," 
                                                             + empName + "," 
                                                             + empDob + "," 
                                                             + empDesignation 
                                                             + "," + empMail 
                                                             + " FROM "
                                                             + "EMPLOYEE");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Employee employee = new Employee(
                                           resultSet.getString(empId),  
                                           resultSet.getString(empDesignation),
                                           resultSet.getString(empMail),
                                           resultSet.getString(empName),
                                           resultSet.getString(empDob));
                    employees.add(employee);
                }
                resultSet.close();
                statement.close();
                return employees;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception "
                                      + "occured while getting employees data",
                                      exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
        return null;
    }
    
    /**
     * <p>
     * Getting Connection
     * </p>
     */
    private void getConnection() {
        try {
            DataBaseConnection dataBaseConnection =  
                                              DataBaseConnection.getInstance();
            connection = dataBaseConnection.getConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception occured"
                                + " while establishing connection", exception);        
        }
    }
    
    /**
     * <p>
     * Closing Connection
     * </p>
     */    
    private void closeConnection() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception occured "
                                + "while closing connection", exception);        
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception occured "
                                + "while closing connection");        
             throw new AppException(Constant.NO_CONNECTION);
        }
    }      
}

