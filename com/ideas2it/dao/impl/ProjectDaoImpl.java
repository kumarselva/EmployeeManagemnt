package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.common.Constant;
import com.ideas2it.connection.DataBaseConnection;
import com.ideas2it.dao.ProjectDao;
import com.ideas2it.exception.AppException;
import com.ideas2it.logger.LoggerInfo;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * project
 * </p>
 * @author Selvakumar
 */
public class ProjectDaoImpl implements ProjectDao {
    private static final String PROJECT_ID = "PROJECT_ID";
    private static final String PROJECT_NAME = "PROJECT_NAME";
    private static final String PROJECT_DOMAIN = "PROJECT_DOMAIN";
    private static final String EMP_ID = "EMPLOYEE_ID";
    private static final String EMP_NAME = "EMPLOYEE_NAME";
    private static final String EMP_MAIL = "EMPLOYEE_MAIL";
    private static final String EMP_DOB = "EMPLOYEE_DOB";
    private static final String EMP_DESIGNATION = "EMPLOYEE_DESIGNATION";     
    private static Connection connection;
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void insertProject(Project project)
     */
    public void insertProject(Project project) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                String sql = "INSERT INTO PROJECT(" + PROJECT_ID + "," 
                                                     + PROJECT_NAME + "," 
                                                     + PROJECT_DOMAIN + ")"
                                                     +  "VALUES(?,?,?)";
                PreparedStatement statement =  connection.prepareStatement(sql);
                statement.setString(1, project.getId());
                statement.setString(2, project.getName());
                statement.setString(3, project.getDomain());            
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, 
                                   "SQL exception occured while inserting data " 
                                   + project);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #Project getProject(String projectId)
     */    
    public Project getProject(String projectId) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                Project project = null;
                PreparedStatement statement =
                         connection.prepareStatement("SELECT " + PROJECT_ID
                                                   + "," + PROJECT_NAME + "," 
                                                   + PROJECT_DOMAIN 
                                                   + " FROM PROJECT " 
                                                   + "WHERE " + PROJECT_ID 
                                                   + " = ?");
                statement.setString(1, projectId);
                ResultSet resultSet = statement.executeQuery( );
                while (resultSet.next()) {
                    project = new Project(
                                          resultSet.getString(PROJECT_ID), 
                                          resultSet.getString(PROJECT_NAME),
                                          resultSet.getString(PROJECT_DOMAIN));
                }
                resultSet.close();
                statement.close();
                return project;
            } 
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                                   + "occured while getting data using id = " 
                                   + projectId);
            throw new AppException();
        } finally {
           closeConnection();
        }
        return null;
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void deleteProject(String projectId)
     */
    public void deleteProject(String projectId) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                              connection.prepareStatement("UPDATE EMPLOYEE SET " 
                                                    + PROJECT_ID + " = null"
                                                    + " WHERE " 
                                                    + PROJECT_ID + "= ?");
                statement.setString(1, projectId);
                statement.executeUpdate();        
                statement =  connection.prepareStatement("DELETE FROM PROJECT "
                                                    + "WHERE " + PROJECT_ID
                                                    + " = ?");
                statement.setString(1, projectId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                                    + "occured while deleting data using "
                                    + "project Id = "  + projectId);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void assignProject(String projectId, String employeeId)
     */    
    public void assignProjectToEmployee(String projectId, String employeeId) 
                                            throws AppException {        
        try {
            getConnection();
            if (null != connection) {
                String query = "UPDATE EMPLOYEE SET " + PROJECT_ID + " = ?" 
                                   + " WHERE " + EMP_ID + " = ?";
                PreparedStatement statement = 
                                             connection.prepareStatement(query);   
                statement.setString(1, projectId);
                statement.setString(2, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                                       + "occured while assigining project to  "
                                       + "employee where project Id = "  
                                       + projectId + " and employee id = " 
                                       + employeeId);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void deleteEmployeeFromProject(String projectId,
     *                                       String employeeId)
     */
    public void deleteEmployeeFromProject(String projectId, String employeeId)
                                    throws AppException {
        try {
            getConnection();
            if (null != connection) {
                String query = "UPDATE EMPLOYEE SET " + PROJECT_ID + " = ?" 
                                   + " WHERE " + EMP_ID + " = ?";
                PreparedStatement statement = 
                                             connection.prepareStatement(query);   
                statement.setString(1, null);
                statement.setString(2, employeeId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                                       + "occured while removing an employee "
                                       + "from project where project Id = "  
                                       + projectId + " and employee id = " 
                                       + employeeId );
           throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void updateProjectName(String projectId, String newName)
     */ 
    public void updateProjectName(String projectId, String newName)
                                    throws AppException {
        updateProject(PROJECT_NAME, projectId, newName);
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void updateProjectDomain(String projectId, String newDomain)
     */    
    public void updateProjectDomain(String projectId, String newDomain)
                                   throws AppException {       
        updateProject(PROJECT_DOMAIN, projectId, newDomain);
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #void updateProject(String tableColumnName, String projectId, 
     *                           String dataToBeUpdated)
     */    
    public void updateProject(String tableColumnName, String projectId, 
                               String dataToBeUpdated) 
                               throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = 
                                         connection.prepareStatement("UPDATE "
                                                  + "PROJECT SET " 
                                                  + "? = ? WHERE " + PROJECT_ID
                                                  + " = ?");   
                statement.setString(1, tableColumnName);
                statement.setString(2, dataToBeUpdated);
                statement.setString(3, projectId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                            + "occured while updating " + dataToBeUpdated
                            + " in column " + tableColumnName  
                            + " by using the project Id = " + projectId);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #List<Project> retrieveProjects()
     */    
    public List<Project> retrieveProjects() throws AppException {
        try {   
            getConnection();
            if (null != connection) {
                List<Project> projects = new ArrayList<Project>();
                PreparedStatement statement =  
                                connection.prepareStatement("SELECT "
                                                           + PROJECT_ID + ","
                                                           + PROJECT_NAME + ","
                                                           + PROJECT_DOMAIN
                                                           + " FROM "
                                                           + "PROJECT");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Project project = new Project(
                                          resultSet.getString(PROJECT_ID),
                                          resultSet.getString(PROJECT_NAME),
                                          resultSet.getString(PROJECT_DOMAIN));
                    projects.add(project);
                }
                resultSet.close();
                statement.close();
                return projects;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                                       + "occured while getting all projects");
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #List<Employee> getEmployeesInProject(String projectId)
     */   
    public List<Employee> getEmployeesInProject(String projectId) 
                                    throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement preparedStatement = 
                                        connection.prepareStatement("SELECT * "
                                                      + "FROM EMPLOYEE WHERE " 
                                                      + PROJECT_ID + " = ? ");
                preparedStatement.setString(1, projectId);
                ResultSet resultSet = preparedStatement.executeQuery();
                List<Employee> employees = new ArrayList<Employee>();
                while (resultSet.next()) {
                    Employee employee = new Employee(
                                           resultSet.getString(EMP_ID),  
                                           resultSet.getString(EMP_DESIGNATION),
                                           resultSet.getString(EMP_MAIL),
                                           resultSet.getString(EMP_NAME),
                                           resultSet.getString(EMP_DOB));
                    employees.add(employee);
                }
                resultSet.close();
                preparedStatement.close();
                return employees;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                            + "occured while getting employees who are"
                            + " involved in a project where project Id = "  
                            + projectId);
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * @See com.ideas2it.dao.ProjectDao;
     * #List<List<String>> getEmployeesNotInProject()
     */    
    public List<List<String>> getEmployeesNotInProject() throws AppException {
        try {
            getConnection();
            if (null != connection) {
                List<List<String>> employees = new ArrayList<List<String>>();
                List<String> employee = null;
                PreparedStatement statement = 
                                        connection.prepareStatement("SELECT " 
                                                            + EMP_ID + "," 
                                                            + EMP_NAME + ","
                                                            + EMP_MAIL 
                                                            + " FROM "
                                                            + "EMPLOYEE WHERE " 
                                                            + PROJECT_ID 
                                                            + " IS "
                                                            + "NULL");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    employee = new ArrayList<String>();
                    employee.add(resultSet.getString(EMP_ID));
                    employee.add(resultSet.getString(EMP_MAIL));
                    employee.add(resultSet.getString(EMP_NAME));
                    employees.add(employee);
                }
                resultSet.close();
                statement.close();
                return employees;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception "
                            + "occured while getting employees who are"
                            + "not involved in any project ");
            throw new AppException();
        } finally {
           closeConnection();
        }
    }
    
    /**
     * <p>
     * Getting Connection
     * </p>
     */
    private void getConnection() {
        try {
            DataBaseConnection dataBaseConnection =  
                                              DataBaseConnection.getInstance();
            connection = dataBaseConnection.getConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception occured while"
                                + " establishing connection");        
        }
    }
    
    /**
     * <p>
     * Closing Connection
     * </p>
     */    
    private void closeConnection() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(ProjectDaoImpl.class, "SQL exception occured while"
                                + " closing connection");        
             throw new AppException();
        }
    }
}

