package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.common.Constant;
import com.ideas2it.connection.DataBaseConnection;
import com.ideas2it.dao.ClientDao;
import com.ideas2it.exception.AppException;
import com.ideas2it.logger.LoggerInfo;
import com.ideas2it.model.Client;

/**
 * <p>
 * This class is used to insert, update and retrieve the records about the 
 * client
 * </p>
 * @author Selvakumar
 */ 
public class ClientDaoImpl implements ClientDao {
    private final String CLIENT_ID = "CLIENT_ID";
    private final String CLIENT_NAME = "CLIENT_NAME";
    private final String CLIENT_MAIL = "CLIENT_MAIL";
    private final String CLIENT_ORGANISATION = "ORGANISATION";
    private final String PROJECT_ID = "PROJECT_ID";    
    private Connection connection;

    /**
     * @com.ideas2it.dao.ClientDao
     * #void insertClient(Client client)
     */
    public boolean insertClient(Client client) throws AppException {        
        try {
            getConnection();
            if (null != connection) {
                String sql = "INSERT INTO CLIENT(" + CLIENT_ID + "," 
                                    + CLIENT_NAME + "," + CLIENT_MAIL + "," 
                                    + CLIENT_ORGANISATION  + ")" 
                                    + "VALUES(?,?,?,?)";
                PreparedStatement statement =  connection.prepareStatement(sql);
                statement.setString(1, client.getId());
                statement.setString(2, client.getName());
                statement.setString(3, client.getMail());
                statement.setString(4, client.getOrganisation());           
                statement.executeUpdate();
                statement.close();
            }
            return true;
        } catch (SQLException exception) {
            LoggerInfo.debug(ClientDaoImpl.class, "SQL exception occured "
                                   + "while inserting data " + client, 
                                   exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
    }

    /**
     * @com.ideas2it.dao.ClientDao
     * #Client getClient(String clientId)
     */    
     public Client getClient(String clientId) throws AppException {
       try {
            getConnection();
            if (null != connection) {
                Client client = null;
                PreparedStatement statement = 
                       connection.prepareStatement("SELECT " + CLIENT_ID + "," 
                                                  + CLIENT_NAME + ","
                                                  + CLIENT_MAIL + ","  
                                                  + CLIENT_ORGANISATION 
                                                  + " FROM "
                                                  + "CLIENT WHERE " 
                                                  + CLIENT_ID
                                                  + " = ?");
                statement.setString(1, clientId);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    client = new Client(resultSet.getString(CLIENT_ID),
                                       resultSet.getString(CLIENT_NAME),  
                                       resultSet.getString(CLIENT_ORGANISATION),
                                       resultSet.getString(CLIENT_MAIL));
                }
                resultSet.close();
                statement.close();
                return client;
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ClientDaoImpl.class, "SQL exception "
                                   + "occured while getting client data by"
                                   + "using id = " + clientId, exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }     
        return null;
    }

    /**
     * @com.ideas2it.dao.ClientDao
     * #void deleteClient(String clientId)
     */    
    public void deleteClient(String clientId) throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement = connection.
                                                  prepareStatement("DELETE FROM"
                                                  + " ADDRESS WHERE "
                                                  + CLIENT_ID + " = ?");
                statement.setString(1, clientId);
                statement.executeUpdate();
                statement = connection.prepareStatement("SELECT PROJECT_ID FROM"
                                                            + " PROJECT WHERE "
                                                            + CLIENT_ID 
                                                            + " = ?");
                statement.setString(1, clientId);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    statement = connection.prepareStatement("UPDATE EMPLOYEE "
                                                               + "SET "
                                                               + "PROJECT_ID"
                                                               + " = NULL "
                                                               + "WHERE "
                                                               + "PROJECT_ID"
                                                               + " = ?");
                    statement.setString(1, resultSet.getString("PROJECT_ID"));
                    statement.executeUpdate();
                }
                statement = connection.prepareStatement("DELETE FROM"
                                                  + " PROJECT WHERE "
                                                  + CLIENT_ID + " = ?");
                statement.setString(1, clientId);
                statement.executeUpdate();
                statement = connection.prepareStatement("DELETE FROM CLIENT "
                                                        + "WHERE " + CLIENT_ID
                                                        + " = ?");
                statement.setString(1, clientId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ClientDaoImpl.class, "SQL exception "
                                    + "occured while deleting client by using "
                                    + "the client Id = "  + clientId,
                                    exception);                                   
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }    
    }

    /**
     * @com.ideas2it.dao.ClientDao
     * #void updateClient(String tableColumnName, 
     *                               String clientId, String dataToBeUpdated)
     */       
    public void updateClientInfo(Client client)
                                throws AppException {
        try {
            getConnection();
            if (null != connection) {
                PreparedStatement statement =  
                                  connection.prepareStatement("UPDATE "
                                                           + "CLIENT SET " 
                                                           + CLIENT_NAME 
                                                           + "= ? "
                                                           + CLIENT_MAIL 
                                                           + "= ? "
                                                           + CLIENT_ORGANISATION 
                                                           + "= ? WHERE"
                                                           + CLIENT_ID
                                                           + "= ?");  
                statement.setString(1, client.getName());
                statement.setString(2, client.getMail());
                statement.setString(3, client.getOrganisation());
                statement.setString(4, client.getId());
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ClientDaoImpl.class, "SQL exception "
                                    + "occured while updating client has the "
                                    + "following data "  + client,
                                    exception);                                           
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }    
    }

    /**
     * @com.ideas2it.dao.ClientDao
     * #List<Client> retrieveClients()
     */    
    public List<Client> retrieveClients() throws AppException {
        try {
            getConnection();
            if (null != connection) {
                List<Client> clients = new ArrayList<Client>();
                PreparedStatement statement = 
                             connection.prepareStatement("SELECT " 
                                                           + CLIENT_ID + "," 
                                                           + CLIENT_NAME + "," 
                                                           + CLIENT_MAIL + "," 
                                                           + CLIENT_ORGANISATION 
                                                           + " FROM "
                                                           + "CLIENT");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Client client = new Client(
                                       resultSet.getString(CLIENT_ID),
                                       resultSet.getString(CLIENT_NAME),  
                                       resultSet.getString(CLIENT_MAIL),
                                       resultSet.
                                                getString(CLIENT_ORGANISATION));
                    clients.add(client);
                }
                resultSet.close();
                statement.close();
                return clients;
            }
            return null;
        } catch (SQLException exception) {
            LoggerInfo.debug(ClientDaoImpl.class, "SQL exception "
                                      + "occured while getting clients data",
                                      exception);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        } 
    }

    /**
     * @com.ideas2it.dao.ClientDao
     * #List<Client> assignProjectToClient(String clientId, string projectId)
     */    
    public void assignProjectToClient(String clientId, String projectId) 
                                                    throws AppException {       
        try {
            getConnection();
            if (null != connection) {
                String query = "UPDATE PROJECT SET " + CLIENT_ID + " = ?" 
                                   + " WHERE " + PROJECT_ID + " = ?";
                PreparedStatement statement = 
                                             connection.prepareStatement(query);   
                statement.setString(1, clientId);
                statement.setString(2, projectId);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException exception) {
            LoggerInfo.debug(ClientDaoImpl.class, "SQL exception "
                                       + "occured while assigining project to  "
                                       + "client where project Id = "  
                                       + projectId + " and client id = " 
                                       + clientId);
            throw new AppException(Constant.NO_CONNECTION);
        } finally {
            closeConnection();
        }
    }
    
    /**
     * <p>
     * Getting Connection
     * </p>
     */
    private void getConnection() {
        try {
            DataBaseConnection dataBaseConnection =  
                                              DataBaseConnection.getInstance();
            connection = dataBaseConnection.getConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception occured"
                                + " while establishing connection", exception);        
        }
    }
    
    /**
     * <p>
     * Closing Connection
     * </p>
     */    
    private void closeConnection() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            LoggerInfo.debug(EmployeeDaoImpl.class, "SQL exception occured while"
                                + " closing connection");        
             throw new AppException(Constant.NO_CONNECTION);
        }
    }  
}

